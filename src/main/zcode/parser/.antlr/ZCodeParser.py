# Generated from c:/Users/HIEU/Desktop/Assignment2/src/main/zcode/parser/ZCode.g4 by ANTLR 4.13.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO

def serializedATN():
    return [
        4,1,51,404,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
        6,2,7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,2,12,7,12,2,13,7,13,
        2,14,7,14,2,15,7,15,2,16,7,16,2,17,7,17,2,18,7,18,2,19,7,19,2,20,
        7,20,2,21,7,21,2,22,7,22,2,23,7,23,2,24,7,24,2,25,7,25,2,26,7,26,
        2,27,7,27,2,28,7,28,2,29,7,29,2,30,7,30,2,31,7,31,2,32,7,32,2,33,
        7,33,2,34,7,34,2,35,7,35,2,36,7,36,2,37,7,37,2,38,7,38,2,39,7,39,
        2,40,7,40,2,41,7,41,2,42,7,42,2,43,7,43,1,0,5,0,90,8,0,10,0,12,0,
        93,9,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,3,1,102,8,1,1,2,1,2,1,2,1,2,3,
        2,108,8,2,1,3,1,3,1,3,3,3,113,8,3,1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,
        4,1,4,1,4,1,4,3,4,126,8,4,1,5,1,5,1,6,1,6,1,6,1,6,1,7,1,7,1,7,1,
        7,1,8,1,8,1,8,3,8,141,8,8,1,9,1,9,1,9,1,10,1,10,1,10,1,10,3,10,150,
        8,10,1,11,1,11,1,11,1,11,1,11,1,11,3,11,158,8,11,1,11,1,11,3,11,
        162,8,11,1,11,1,11,3,11,166,8,11,1,12,1,12,3,12,170,8,12,1,13,1,
        13,1,13,1,13,1,13,3,13,177,8,13,1,14,1,14,1,14,1,14,1,14,1,14,1,
        14,3,14,186,8,14,1,15,1,15,1,15,1,15,3,15,192,8,15,1,16,1,16,1,16,
        1,16,1,16,1,16,1,16,1,16,1,16,3,16,203,8,16,1,17,1,17,1,17,1,18,
        1,18,1,18,3,18,211,8,18,1,18,1,18,1,18,1,18,1,19,1,19,1,19,1,19,
        1,20,1,20,1,20,1,20,3,20,225,8,20,1,20,1,20,3,20,229,8,20,1,21,1,
        21,1,21,1,21,1,21,1,21,3,21,237,8,21,1,22,1,22,1,22,1,22,1,22,1,
        22,3,22,245,8,22,1,23,1,23,1,23,1,23,1,23,3,23,252,8,23,1,24,1,24,
        1,24,1,24,1,25,1,25,1,25,1,25,1,25,1,25,1,25,1,25,1,25,1,25,3,25,
        268,8,25,1,26,1,26,1,26,1,27,1,27,1,27,1,28,1,28,1,28,3,28,279,8,
        28,1,28,1,28,1,29,1,29,1,29,1,29,3,29,287,8,29,1,29,1,29,1,29,1,
        30,1,30,1,30,1,30,3,30,296,8,30,1,30,1,30,1,31,1,31,1,31,1,31,3,
        31,304,8,31,1,31,1,31,1,31,1,32,1,32,1,32,1,32,1,32,3,32,314,8,32,
        1,33,1,33,1,33,1,33,1,33,3,33,321,8,33,1,34,1,34,1,34,1,34,1,34,
        3,34,328,8,34,1,35,1,35,1,35,1,35,1,35,1,35,5,35,336,8,35,10,35,
        12,35,339,9,35,1,36,1,36,1,36,1,36,1,36,1,36,5,36,347,8,36,10,36,
        12,36,350,9,36,1,37,1,37,1,37,1,37,1,37,1,37,5,37,358,8,37,10,37,
        12,37,361,9,37,1,38,1,38,1,38,3,38,366,8,38,1,39,1,39,1,39,1,39,
        3,39,372,8,39,1,40,1,40,3,40,376,8,40,1,40,1,40,1,40,1,40,1,41,1,
        41,1,41,1,41,1,41,1,41,1,41,1,41,1,41,1,41,1,41,3,41,393,8,41,1,
        42,1,42,1,42,1,42,1,43,4,43,400,8,43,11,43,12,43,401,1,43,0,3,70,
        72,74,44,0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,
        40,42,44,46,48,50,52,54,56,58,60,62,64,66,68,70,72,74,76,78,80,82,
        84,86,0,5,1,0,3,5,3,0,25,25,27,31,33,33,1,0,34,35,1,0,20,21,1,0,
        22,24,412,0,91,1,0,0,0,2,101,1,0,0,0,4,107,1,0,0,0,6,112,1,0,0,0,
        8,125,1,0,0,0,10,127,1,0,0,0,12,129,1,0,0,0,14,133,1,0,0,0,16,140,
        1,0,0,0,18,142,1,0,0,0,20,149,1,0,0,0,22,151,1,0,0,0,24,169,1,0,
        0,0,26,176,1,0,0,0,28,178,1,0,0,0,30,191,1,0,0,0,32,202,1,0,0,0,
        34,204,1,0,0,0,36,210,1,0,0,0,38,216,1,0,0,0,40,220,1,0,0,0,42,236,
        1,0,0,0,44,244,1,0,0,0,46,246,1,0,0,0,48,253,1,0,0,0,50,257,1,0,
        0,0,52,269,1,0,0,0,54,272,1,0,0,0,56,275,1,0,0,0,58,282,1,0,0,0,
        60,291,1,0,0,0,62,299,1,0,0,0,64,313,1,0,0,0,66,320,1,0,0,0,68,327,
        1,0,0,0,70,329,1,0,0,0,72,340,1,0,0,0,74,351,1,0,0,0,76,365,1,0,
        0,0,78,371,1,0,0,0,80,375,1,0,0,0,82,392,1,0,0,0,84,394,1,0,0,0,
        86,399,1,0,0,0,88,90,5,46,0,0,89,88,1,0,0,0,90,93,1,0,0,0,91,89,
        1,0,0,0,91,92,1,0,0,0,92,94,1,0,0,0,93,91,1,0,0,0,94,95,3,2,1,0,
        95,96,5,0,0,1,96,1,1,0,0,0,97,98,3,4,2,0,98,99,3,2,1,0,99,102,1,
        0,0,0,100,102,3,4,2,0,101,97,1,0,0,0,101,100,1,0,0,0,102,3,1,0,0,
        0,103,108,3,22,11,0,104,105,3,6,3,0,105,106,3,86,43,0,106,108,1,
        0,0,0,107,103,1,0,0,0,107,104,1,0,0,0,108,5,1,0,0,0,109,113,3,8,
        4,0,110,113,3,12,6,0,111,113,3,14,7,0,112,109,1,0,0,0,112,110,1,
        0,0,0,112,111,1,0,0,0,113,7,1,0,0,0,114,115,3,10,5,0,115,116,5,42,
        0,0,116,117,3,16,8,0,117,126,1,0,0,0,118,119,3,10,5,0,119,120,5,
        42,0,0,120,121,5,39,0,0,121,122,3,20,10,0,122,123,5,40,0,0,123,124,
        3,16,8,0,124,126,1,0,0,0,125,114,1,0,0,0,125,118,1,0,0,0,126,9,1,
        0,0,0,127,128,7,0,0,0,128,11,1,0,0,0,129,130,5,7,0,0,130,131,5,42,
        0,0,131,132,3,18,9,0,132,13,1,0,0,0,133,134,5,8,0,0,134,135,5,42,
        0,0,135,136,3,16,8,0,136,15,1,0,0,0,137,138,5,26,0,0,138,141,3,66,
        33,0,139,141,1,0,0,0,140,137,1,0,0,0,140,139,1,0,0,0,141,17,1,0,
        0,0,142,143,5,26,0,0,143,144,3,66,33,0,144,19,1,0,0,0,145,146,5,
        45,0,0,146,147,5,41,0,0,147,150,3,20,10,0,148,150,5,45,0,0,149,145,
        1,0,0,0,149,148,1,0,0,0,150,21,1,0,0,0,151,152,5,9,0,0,152,153,5,
        42,0,0,153,154,5,37,0,0,154,155,3,24,12,0,155,165,5,38,0,0,156,158,
        3,86,43,0,157,156,1,0,0,0,157,158,1,0,0,0,158,159,1,0,0,0,159,166,
        3,58,29,0,160,162,3,86,43,0,161,160,1,0,0,0,161,162,1,0,0,0,162,
        163,1,0,0,0,163,166,3,56,28,0,164,166,3,86,43,0,165,157,1,0,0,0,
        165,161,1,0,0,0,165,164,1,0,0,0,166,23,1,0,0,0,167,170,3,26,13,0,
        168,170,1,0,0,0,169,167,1,0,0,0,169,168,1,0,0,0,170,25,1,0,0,0,171,
        172,3,28,14,0,172,173,5,41,0,0,173,174,3,26,13,0,174,177,1,0,0,0,
        175,177,3,28,14,0,176,171,1,0,0,0,176,175,1,0,0,0,177,27,1,0,0,0,
        178,185,3,10,5,0,179,186,5,42,0,0,180,181,5,42,0,0,181,182,5,39,
        0,0,182,183,3,20,10,0,183,184,5,40,0,0,184,186,1,0,0,0,185,179,1,
        0,0,0,185,180,1,0,0,0,186,29,1,0,0,0,187,188,3,32,16,0,188,189,3,
        30,15,0,189,192,1,0,0,0,190,192,1,0,0,0,191,187,1,0,0,0,191,190,
        1,0,0,0,192,31,1,0,0,0,193,203,3,34,17,0,194,203,3,36,18,0,195,203,
        3,40,20,0,196,203,3,50,25,0,197,203,3,54,27,0,198,203,3,52,26,0,
        199,203,3,56,28,0,200,203,3,62,31,0,201,203,3,58,29,0,202,193,1,
        0,0,0,202,194,1,0,0,0,202,195,1,0,0,0,202,196,1,0,0,0,202,197,1,
        0,0,0,202,198,1,0,0,0,202,199,1,0,0,0,202,200,1,0,0,0,202,201,1,
        0,0,0,203,33,1,0,0,0,204,205,3,6,3,0,205,206,3,86,43,0,206,35,1,
        0,0,0,207,211,5,42,0,0,208,209,5,42,0,0,209,211,3,38,19,0,210,207,
        1,0,0,0,210,208,1,0,0,0,211,212,1,0,0,0,212,213,5,26,0,0,213,214,
        3,66,33,0,214,215,3,86,43,0,215,37,1,0,0,0,216,217,5,39,0,0,217,
        218,3,64,32,0,218,219,5,40,0,0,219,39,1,0,0,0,220,221,5,15,0,0,221,
        224,3,46,23,0,222,225,3,42,21,0,223,225,1,0,0,0,224,222,1,0,0,0,
        224,223,1,0,0,0,225,228,1,0,0,0,226,229,3,44,22,0,227,229,1,0,0,
        0,228,226,1,0,0,0,228,227,1,0,0,0,229,41,1,0,0,0,230,231,5,17,0,
        0,231,232,3,46,23,0,232,233,3,42,21,0,233,237,1,0,0,0,234,235,5,
        17,0,0,235,237,3,46,23,0,236,230,1,0,0,0,236,234,1,0,0,0,237,43,
        1,0,0,0,238,239,5,16,0,0,239,240,3,86,43,0,240,241,3,32,16,0,241,
        245,1,0,0,0,242,243,5,16,0,0,243,245,3,32,16,0,244,238,1,0,0,0,244,
        242,1,0,0,0,245,45,1,0,0,0,246,251,3,48,24,0,247,248,3,86,43,0,248,
        249,3,32,16,0,249,252,1,0,0,0,250,252,3,32,16,0,251,247,1,0,0,0,
        251,250,1,0,0,0,252,47,1,0,0,0,253,254,5,37,0,0,254,255,3,66,33,
        0,255,256,5,38,0,0,256,49,1,0,0,0,257,258,5,10,0,0,258,259,5,42,
        0,0,259,260,5,11,0,0,260,261,3,66,33,0,261,262,5,12,0,0,262,267,
        3,66,33,0,263,268,3,32,16,0,264,265,3,86,43,0,265,266,3,32,16,0,
        266,268,1,0,0,0,267,263,1,0,0,0,267,264,1,0,0,0,268,51,1,0,0,0,269,
        270,5,14,0,0,270,271,3,86,43,0,271,53,1,0,0,0,272,273,5,13,0,0,273,
        274,3,86,43,0,274,55,1,0,0,0,275,278,5,6,0,0,276,279,3,66,33,0,277,
        279,1,0,0,0,278,276,1,0,0,0,278,277,1,0,0,0,279,280,1,0,0,0,280,
        281,3,86,43,0,281,57,1,0,0,0,282,283,5,18,0,0,283,286,3,86,43,0,
        284,287,3,30,15,0,285,287,1,0,0,0,286,284,1,0,0,0,286,285,1,0,0,
        0,287,288,1,0,0,0,288,289,5,19,0,0,289,290,3,86,43,0,290,59,1,0,
        0,0,291,292,5,42,0,0,292,295,5,37,0,0,293,296,3,64,32,0,294,296,
        1,0,0,0,295,293,1,0,0,0,295,294,1,0,0,0,296,297,1,0,0,0,297,298,
        5,38,0,0,298,61,1,0,0,0,299,300,5,42,0,0,300,303,5,37,0,0,301,304,
        3,64,32,0,302,304,1,0,0,0,303,301,1,0,0,0,303,302,1,0,0,0,304,305,
        1,0,0,0,305,306,5,38,0,0,306,307,3,86,43,0,307,63,1,0,0,0,308,309,
        3,66,33,0,309,310,5,41,0,0,310,311,3,64,32,0,311,314,1,0,0,0,312,
        314,3,66,33,0,313,308,1,0,0,0,313,312,1,0,0,0,314,65,1,0,0,0,315,
        316,3,68,34,0,316,317,5,32,0,0,317,318,3,68,34,0,318,321,1,0,0,0,
        319,321,3,68,34,0,320,315,1,0,0,0,320,319,1,0,0,0,321,67,1,0,0,0,
        322,323,3,70,35,0,323,324,7,1,0,0,324,325,3,70,35,0,325,328,1,0,
        0,0,326,328,3,70,35,0,327,322,1,0,0,0,327,326,1,0,0,0,328,69,1,0,
        0,0,329,330,6,35,-1,0,330,331,3,72,36,0,331,337,1,0,0,0,332,333,
        10,2,0,0,333,334,7,2,0,0,334,336,3,72,36,0,335,332,1,0,0,0,336,339,
        1,0,0,0,337,335,1,0,0,0,337,338,1,0,0,0,338,71,1,0,0,0,339,337,1,
        0,0,0,340,341,6,36,-1,0,341,342,3,74,37,0,342,348,1,0,0,0,343,344,
        10,2,0,0,344,345,7,3,0,0,345,347,3,74,37,0,346,343,1,0,0,0,347,350,
        1,0,0,0,348,346,1,0,0,0,348,349,1,0,0,0,349,73,1,0,0,0,350,348,1,
        0,0,0,351,352,6,37,-1,0,352,353,3,76,38,0,353,359,1,0,0,0,354,355,
        10,2,0,0,355,356,7,4,0,0,356,358,3,76,38,0,357,354,1,0,0,0,358,361,
        1,0,0,0,359,357,1,0,0,0,359,360,1,0,0,0,360,75,1,0,0,0,361,359,1,
        0,0,0,362,363,5,36,0,0,363,366,3,76,38,0,364,366,3,78,39,0,365,362,
        1,0,0,0,365,364,1,0,0,0,366,77,1,0,0,0,367,368,5,21,0,0,368,372,
        3,78,39,0,369,372,3,80,40,0,370,372,3,82,41,0,371,367,1,0,0,0,371,
        369,1,0,0,0,371,370,1,0,0,0,372,79,1,0,0,0,373,376,5,42,0,0,374,
        376,3,60,30,0,375,373,1,0,0,0,375,374,1,0,0,0,376,377,1,0,0,0,377,
        378,5,39,0,0,378,379,3,64,32,0,379,380,5,40,0,0,380,81,1,0,0,0,381,
        393,5,45,0,0,382,393,5,44,0,0,383,393,5,1,0,0,384,393,5,2,0,0,385,
        393,5,42,0,0,386,393,3,84,42,0,387,388,5,37,0,0,388,389,3,66,33,
        0,389,390,5,38,0,0,390,393,1,0,0,0,391,393,3,60,30,0,392,381,1,0,
        0,0,392,382,1,0,0,0,392,383,1,0,0,0,392,384,1,0,0,0,392,385,1,0,
        0,0,392,386,1,0,0,0,392,387,1,0,0,0,392,391,1,0,0,0,393,83,1,0,0,
        0,394,395,5,39,0,0,395,396,3,64,32,0,396,397,5,40,0,0,397,85,1,0,
        0,0,398,400,5,46,0,0,399,398,1,0,0,0,400,401,1,0,0,0,401,399,1,0,
        0,0,401,402,1,0,0,0,402,87,1,0,0,0,37,91,101,107,112,125,140,149,
        157,161,165,169,176,185,191,202,210,224,228,236,244,251,267,278,
        286,295,303,313,320,327,337,348,359,365,371,375,392,401
    ]

class ZCodeParser ( Parser ):

    grammarFileName = "ZCode.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'true'", "'false'", "'number'", "'bool'", 
                     "'string'", "'return'", "'var'", "'dynamic'", "'func'", 
                     "'for'", "'until'", "'by'", "'break'", "'continue'", 
                     "'if'", "'else'", "'elif'", "'begin'", "'end'", "'+'", 
                     "'-'", "'*'", "'/'", "'%'", "'='", "'<-'", "'!='", 
                     "'<'", "'<='", "'>'", "'>='", "'...'", "'=='", "'and'", 
                     "'or'", "'not'", "'('", "')'", "'['", "']'", "','" ]

    symbolicNames = [ "<INVALID>", "TRUE", "FALSE", "NUMBER", "BOOL", "STRING", 
                      "RETURN", "VAR", "DYNAMIC", "FUNC", "FOR", "UNTIL", 
                      "BY", "BREAK", "CONTINUE", "IF", "ELSE", "ELIF", "BEGIN", 
                      "END", "ADD", "SUB", "MUL", "DIV", "MOD", "EQ", "ASSIGN", 
                      "NEQ", "LT", "LTE", "GT", "GTE", "CONCAT", "EQEQ", 
                      "AND", "OR", "NOT", "LPAREN", "RPAREN", "LBRACK", 
                      "RBRACK", "COMMA", "ID", "BOOLEAN", "STRINGLIT", "NUMBERLIT", 
                      "NEWLINE", "WS", "COMMENTS", "UNCLOSE_STRING", "ILLEGAL_ESCAPE", 
                      "ERROR_CHAR" ]

    RULE_program = 0
    RULE_listDeclared = 1
    RULE_declared = 2
    RULE_variablesDeclared = 3
    RULE_explicitTypeDeclared = 4
    RULE_explicitType = 5
    RULE_implicitVarDeclared = 6
    RULE_implicitDynamicDeclared = 7
    RULE_initVariableValueOrNull = 8
    RULE_initVariableValue = 9
    RULE_numbersList = 10
    RULE_functionDeclared = 11
    RULE_prametersList = 12
    RULE_paramsPrime = 13
    RULE_param = 14
    RULE_statementList = 15
    RULE_statement = 16
    RULE_declaredStatement = 17
    RULE_assignmentStatement = 18
    RULE_indexOperatorNew = 19
    RULE_conditionStatement = 20
    RULE_elseIfStatement = 21
    RULE_elseStatement = 22
    RULE_conditionAndStatement = 23
    RULE_conditionExpress = 24
    RULE_forStatement = 25
    RULE_continueStatement = 26
    RULE_breakStatement = 27
    RULE_returnStatement = 28
    RULE_blockStatement = 29
    RULE_callFunction = 30
    RULE_callStatement = 31
    RULE_expressList = 32
    RULE_express = 33
    RULE_express1 = 34
    RULE_express2 = 35
    RULE_express3 = 36
    RULE_express4 = 37
    RULE_express5 = 38
    RULE_express6 = 39
    RULE_express7 = 40
    RULE_express8 = 41
    RULE_arrayLiteral = 42
    RULE_ignore = 43

    ruleNames =  [ "program", "listDeclared", "declared", "variablesDeclared", 
                   "explicitTypeDeclared", "explicitType", "implicitVarDeclared", 
                   "implicitDynamicDeclared", "initVariableValueOrNull", 
                   "initVariableValue", "numbersList", "functionDeclared", 
                   "prametersList", "paramsPrime", "param", "statementList", 
                   "statement", "declaredStatement", "assignmentStatement", 
                   "indexOperatorNew", "conditionStatement", "elseIfStatement", 
                   "elseStatement", "conditionAndStatement", "conditionExpress", 
                   "forStatement", "continueStatement", "breakStatement", 
                   "returnStatement", "blockStatement", "callFunction", 
                   "callStatement", "expressList", "express", "express1", 
                   "express2", "express3", "express4", "express5", "express6", 
                   "express7", "express8", "arrayLiteral", "ignore" ]

    EOF = Token.EOF
    TRUE=1
    FALSE=2
    NUMBER=3
    BOOL=4
    STRING=5
    RETURN=6
    VAR=7
    DYNAMIC=8
    FUNC=9
    FOR=10
    UNTIL=11
    BY=12
    BREAK=13
    CONTINUE=14
    IF=15
    ELSE=16
    ELIF=17
    BEGIN=18
    END=19
    ADD=20
    SUB=21
    MUL=22
    DIV=23
    MOD=24
    EQ=25
    ASSIGN=26
    NEQ=27
    LT=28
    LTE=29
    GT=30
    GTE=31
    CONCAT=32
    EQEQ=33
    AND=34
    OR=35
    NOT=36
    LPAREN=37
    RPAREN=38
    LBRACK=39
    RBRACK=40
    COMMA=41
    ID=42
    BOOLEAN=43
    STRINGLIT=44
    NUMBERLIT=45
    NEWLINE=46
    WS=47
    COMMENTS=48
    UNCLOSE_STRING=49
    ILLEGAL_ESCAPE=50
    ERROR_CHAR=51

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.13.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def listDeclared(self):
            return self.getTypedRuleContext(ZCodeParser.ListDeclaredContext,0)


        def EOF(self):
            return self.getToken(ZCodeParser.EOF, 0)

        def NEWLINE(self, i:int=None):
            if i is None:
                return self.getTokens(ZCodeParser.NEWLINE)
            else:
                return self.getToken(ZCodeParser.NEWLINE, i)

        def getRuleIndex(self):
            return ZCodeParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)




    def program(self):

        localctx = ZCodeParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 91
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==46:
                self.state = 88
                self.match(ZCodeParser.NEWLINE)
                self.state = 93
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 94
            self.listDeclared()
            self.state = 95
            self.match(ZCodeParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ListDeclaredContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def declared(self):
            return self.getTypedRuleContext(ZCodeParser.DeclaredContext,0)


        def listDeclared(self):
            return self.getTypedRuleContext(ZCodeParser.ListDeclaredContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_listDeclared

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterListDeclared" ):
                listener.enterListDeclared(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitListDeclared" ):
                listener.exitListDeclared(self)




    def listDeclared(self):

        localctx = ZCodeParser.ListDeclaredContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_listDeclared)
        try:
            self.state = 101
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 97
                self.declared()
                self.state = 98
                self.listDeclared()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 100
                self.declared()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DeclaredContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def functionDeclared(self):
            return self.getTypedRuleContext(ZCodeParser.FunctionDeclaredContext,0)


        def variablesDeclared(self):
            return self.getTypedRuleContext(ZCodeParser.VariablesDeclaredContext,0)


        def ignore(self):
            return self.getTypedRuleContext(ZCodeParser.IgnoreContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_declared

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDeclared" ):
                listener.enterDeclared(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDeclared" ):
                listener.exitDeclared(self)




    def declared(self):

        localctx = ZCodeParser.DeclaredContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_declared)
        try:
            self.state = 107
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [9]:
                self.enterOuterAlt(localctx, 1)
                self.state = 103
                self.functionDeclared()
                pass
            elif token in [3, 4, 5, 7, 8]:
                self.enterOuterAlt(localctx, 2)
                self.state = 104
                self.variablesDeclared()
                self.state = 105
                self.ignore()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VariablesDeclaredContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def explicitTypeDeclared(self):
            return self.getTypedRuleContext(ZCodeParser.ExplicitTypeDeclaredContext,0)


        def implicitVarDeclared(self):
            return self.getTypedRuleContext(ZCodeParser.ImplicitVarDeclaredContext,0)


        def implicitDynamicDeclared(self):
            return self.getTypedRuleContext(ZCodeParser.ImplicitDynamicDeclaredContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_variablesDeclared

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVariablesDeclared" ):
                listener.enterVariablesDeclared(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVariablesDeclared" ):
                listener.exitVariablesDeclared(self)




    def variablesDeclared(self):

        localctx = ZCodeParser.VariablesDeclaredContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_variablesDeclared)
        try:
            self.state = 112
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [3, 4, 5]:
                self.enterOuterAlt(localctx, 1)
                self.state = 109
                self.explicitTypeDeclared()
                pass
            elif token in [7]:
                self.enterOuterAlt(localctx, 2)
                self.state = 110
                self.implicitVarDeclared()
                pass
            elif token in [8]:
                self.enterOuterAlt(localctx, 3)
                self.state = 111
                self.implicitDynamicDeclared()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExplicitTypeDeclaredContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def explicitType(self):
            return self.getTypedRuleContext(ZCodeParser.ExplicitTypeContext,0)


        def ID(self):
            return self.getToken(ZCodeParser.ID, 0)

        def initVariableValueOrNull(self):
            return self.getTypedRuleContext(ZCodeParser.InitVariableValueOrNullContext,0)


        def LBRACK(self):
            return self.getToken(ZCodeParser.LBRACK, 0)

        def numbersList(self):
            return self.getTypedRuleContext(ZCodeParser.NumbersListContext,0)


        def RBRACK(self):
            return self.getToken(ZCodeParser.RBRACK, 0)

        def getRuleIndex(self):
            return ZCodeParser.RULE_explicitTypeDeclared

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExplicitTypeDeclared" ):
                listener.enterExplicitTypeDeclared(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExplicitTypeDeclared" ):
                listener.exitExplicitTypeDeclared(self)




    def explicitTypeDeclared(self):

        localctx = ZCodeParser.ExplicitTypeDeclaredContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_explicitTypeDeclared)
        try:
            self.state = 125
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 114
                self.explicitType()
                self.state = 115
                self.match(ZCodeParser.ID)
                self.state = 116
                self.initVariableValueOrNull()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 118
                self.explicitType()
                self.state = 119
                self.match(ZCodeParser.ID)
                self.state = 120
                self.match(ZCodeParser.LBRACK)
                self.state = 121
                self.numbersList()
                self.state = 122
                self.match(ZCodeParser.RBRACK)
                self.state = 123
                self.initVariableValueOrNull()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExplicitTypeContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRING(self):
            return self.getToken(ZCodeParser.STRING, 0)

        def BOOL(self):
            return self.getToken(ZCodeParser.BOOL, 0)

        def NUMBER(self):
            return self.getToken(ZCodeParser.NUMBER, 0)

        def getRuleIndex(self):
            return ZCodeParser.RULE_explicitType

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExplicitType" ):
                listener.enterExplicitType(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExplicitType" ):
                listener.exitExplicitType(self)




    def explicitType(self):

        localctx = ZCodeParser.ExplicitTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_explicitType)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 127
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 56) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ImplicitVarDeclaredContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VAR(self):
            return self.getToken(ZCodeParser.VAR, 0)

        def ID(self):
            return self.getToken(ZCodeParser.ID, 0)

        def initVariableValue(self):
            return self.getTypedRuleContext(ZCodeParser.InitVariableValueContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_implicitVarDeclared

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImplicitVarDeclared" ):
                listener.enterImplicitVarDeclared(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImplicitVarDeclared" ):
                listener.exitImplicitVarDeclared(self)




    def implicitVarDeclared(self):

        localctx = ZCodeParser.ImplicitVarDeclaredContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_implicitVarDeclared)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 129
            self.match(ZCodeParser.VAR)
            self.state = 130
            self.match(ZCodeParser.ID)
            self.state = 131
            self.initVariableValue()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ImplicitDynamicDeclaredContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DYNAMIC(self):
            return self.getToken(ZCodeParser.DYNAMIC, 0)

        def ID(self):
            return self.getToken(ZCodeParser.ID, 0)

        def initVariableValueOrNull(self):
            return self.getTypedRuleContext(ZCodeParser.InitVariableValueOrNullContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_implicitDynamicDeclared

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImplicitDynamicDeclared" ):
                listener.enterImplicitDynamicDeclared(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImplicitDynamicDeclared" ):
                listener.exitImplicitDynamicDeclared(self)




    def implicitDynamicDeclared(self):

        localctx = ZCodeParser.ImplicitDynamicDeclaredContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_implicitDynamicDeclared)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 133
            self.match(ZCodeParser.DYNAMIC)
            self.state = 134
            self.match(ZCodeParser.ID)
            self.state = 135
            self.initVariableValueOrNull()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InitVariableValueOrNullContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ASSIGN(self):
            return self.getToken(ZCodeParser.ASSIGN, 0)

        def express(self):
            return self.getTypedRuleContext(ZCodeParser.ExpressContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_initVariableValueOrNull

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInitVariableValueOrNull" ):
                listener.enterInitVariableValueOrNull(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInitVariableValueOrNull" ):
                listener.exitInitVariableValueOrNull(self)




    def initVariableValueOrNull(self):

        localctx = ZCodeParser.InitVariableValueOrNullContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_initVariableValueOrNull)
        try:
            self.state = 140
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [26]:
                self.enterOuterAlt(localctx, 1)
                self.state = 137
                self.match(ZCodeParser.ASSIGN)
                self.state = 138
                self.express()
                pass
            elif token in [46]:
                self.enterOuterAlt(localctx, 2)

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InitVariableValueContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ASSIGN(self):
            return self.getToken(ZCodeParser.ASSIGN, 0)

        def express(self):
            return self.getTypedRuleContext(ZCodeParser.ExpressContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_initVariableValue

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInitVariableValue" ):
                listener.enterInitVariableValue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInitVariableValue" ):
                listener.exitInitVariableValue(self)




    def initVariableValue(self):

        localctx = ZCodeParser.InitVariableValueContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_initVariableValue)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 142
            self.match(ZCodeParser.ASSIGN)
            self.state = 143
            self.express()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NumbersListContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBERLIT(self):
            return self.getToken(ZCodeParser.NUMBERLIT, 0)

        def COMMA(self):
            return self.getToken(ZCodeParser.COMMA, 0)

        def numbersList(self):
            return self.getTypedRuleContext(ZCodeParser.NumbersListContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_numbersList

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNumbersList" ):
                listener.enterNumbersList(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNumbersList" ):
                listener.exitNumbersList(self)




    def numbersList(self):

        localctx = ZCodeParser.NumbersListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_numbersList)
        try:
            self.state = 149
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 145
                self.match(ZCodeParser.NUMBERLIT)
                self.state = 146
                self.match(ZCodeParser.COMMA)
                self.state = 147
                self.numbersList()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 148
                self.match(ZCodeParser.NUMBERLIT)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctionDeclaredContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FUNC(self):
            return self.getToken(ZCodeParser.FUNC, 0)

        def ID(self):
            return self.getToken(ZCodeParser.ID, 0)

        def LPAREN(self):
            return self.getToken(ZCodeParser.LPAREN, 0)

        def prametersList(self):
            return self.getTypedRuleContext(ZCodeParser.PrametersListContext,0)


        def RPAREN(self):
            return self.getToken(ZCodeParser.RPAREN, 0)

        def blockStatement(self):
            return self.getTypedRuleContext(ZCodeParser.BlockStatementContext,0)


        def returnStatement(self):
            return self.getTypedRuleContext(ZCodeParser.ReturnStatementContext,0)


        def ignore(self):
            return self.getTypedRuleContext(ZCodeParser.IgnoreContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_functionDeclared

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctionDeclared" ):
                listener.enterFunctionDeclared(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctionDeclared" ):
                listener.exitFunctionDeclared(self)




    def functionDeclared(self):

        localctx = ZCodeParser.FunctionDeclaredContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_functionDeclared)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 151
            self.match(ZCodeParser.FUNC)
            self.state = 152
            self.match(ZCodeParser.ID)
            self.state = 153
            self.match(ZCodeParser.LPAREN)
            self.state = 154
            self.prametersList()
            self.state = 155
            self.match(ZCodeParser.RPAREN)
            self.state = 165
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,9,self._ctx)
            if la_ == 1:
                self.state = 157
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==46:
                    self.state = 156
                    self.ignore()


                self.state = 159
                self.blockStatement()
                pass

            elif la_ == 2:
                self.state = 161
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==46:
                    self.state = 160
                    self.ignore()


                self.state = 163
                self.returnStatement()
                pass

            elif la_ == 3:
                self.state = 164
                self.ignore()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrametersListContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def paramsPrime(self):
            return self.getTypedRuleContext(ZCodeParser.ParamsPrimeContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_prametersList

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrametersList" ):
                listener.enterPrametersList(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrametersList" ):
                listener.exitPrametersList(self)




    def prametersList(self):

        localctx = ZCodeParser.PrametersListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_prametersList)
        try:
            self.state = 169
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [3, 4, 5]:
                self.enterOuterAlt(localctx, 1)
                self.state = 167
                self.paramsPrime()
                pass
            elif token in [38]:
                self.enterOuterAlt(localctx, 2)

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParamsPrimeContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def param(self):
            return self.getTypedRuleContext(ZCodeParser.ParamContext,0)


        def COMMA(self):
            return self.getToken(ZCodeParser.COMMA, 0)

        def paramsPrime(self):
            return self.getTypedRuleContext(ZCodeParser.ParamsPrimeContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_paramsPrime

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParamsPrime" ):
                listener.enterParamsPrime(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParamsPrime" ):
                listener.exitParamsPrime(self)




    def paramsPrime(self):

        localctx = ZCodeParser.ParamsPrimeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_paramsPrime)
        try:
            self.state = 176
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,11,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 171
                self.param()
                self.state = 172
                self.match(ZCodeParser.COMMA)
                self.state = 173
                self.paramsPrime()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 175
                self.param()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParamContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def explicitType(self):
            return self.getTypedRuleContext(ZCodeParser.ExplicitTypeContext,0)


        def ID(self):
            return self.getToken(ZCodeParser.ID, 0)

        def LBRACK(self):
            return self.getToken(ZCodeParser.LBRACK, 0)

        def numbersList(self):
            return self.getTypedRuleContext(ZCodeParser.NumbersListContext,0)


        def RBRACK(self):
            return self.getToken(ZCodeParser.RBRACK, 0)

        def getRuleIndex(self):
            return ZCodeParser.RULE_param

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParam" ):
                listener.enterParam(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParam" ):
                listener.exitParam(self)




    def param(self):

        localctx = ZCodeParser.ParamContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_param)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 178
            self.explicitType()
            self.state = 185
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,12,self._ctx)
            if la_ == 1:
                self.state = 179
                self.match(ZCodeParser.ID)
                pass

            elif la_ == 2:
                self.state = 180
                self.match(ZCodeParser.ID)
                self.state = 181
                self.match(ZCodeParser.LBRACK)
                self.state = 182
                self.numbersList()
                self.state = 183
                self.match(ZCodeParser.RBRACK)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementListContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self):
            return self.getTypedRuleContext(ZCodeParser.StatementContext,0)


        def statementList(self):
            return self.getTypedRuleContext(ZCodeParser.StatementListContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_statementList

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatementList" ):
                listener.enterStatementList(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatementList" ):
                listener.exitStatementList(self)




    def statementList(self):

        localctx = ZCodeParser.StatementListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_statementList)
        try:
            self.state = 191
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [3, 4, 5, 6, 7, 8, 10, 13, 14, 15, 18, 42]:
                self.enterOuterAlt(localctx, 1)
                self.state = 187
                self.statement()
                self.state = 188
                self.statementList()
                pass
            elif token in [19]:
                self.enterOuterAlt(localctx, 2)

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def declaredStatement(self):
            return self.getTypedRuleContext(ZCodeParser.DeclaredStatementContext,0)


        def assignmentStatement(self):
            return self.getTypedRuleContext(ZCodeParser.AssignmentStatementContext,0)


        def conditionStatement(self):
            return self.getTypedRuleContext(ZCodeParser.ConditionStatementContext,0)


        def forStatement(self):
            return self.getTypedRuleContext(ZCodeParser.ForStatementContext,0)


        def breakStatement(self):
            return self.getTypedRuleContext(ZCodeParser.BreakStatementContext,0)


        def continueStatement(self):
            return self.getTypedRuleContext(ZCodeParser.ContinueStatementContext,0)


        def returnStatement(self):
            return self.getTypedRuleContext(ZCodeParser.ReturnStatementContext,0)


        def callStatement(self):
            return self.getTypedRuleContext(ZCodeParser.CallStatementContext,0)


        def blockStatement(self):
            return self.getTypedRuleContext(ZCodeParser.BlockStatementContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = ZCodeParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_statement)
        try:
            self.state = 202
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,14,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 193
                self.declaredStatement()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 194
                self.assignmentStatement()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 195
                self.conditionStatement()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 196
                self.forStatement()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 197
                self.breakStatement()
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 198
                self.continueStatement()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 199
                self.returnStatement()
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 200
                self.callStatement()
                pass

            elif la_ == 9:
                self.enterOuterAlt(localctx, 9)
                self.state = 201
                self.blockStatement()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DeclaredStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def variablesDeclared(self):
            return self.getTypedRuleContext(ZCodeParser.VariablesDeclaredContext,0)


        def ignore(self):
            return self.getTypedRuleContext(ZCodeParser.IgnoreContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_declaredStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDeclaredStatement" ):
                listener.enterDeclaredStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDeclaredStatement" ):
                listener.exitDeclaredStatement(self)




    def declaredStatement(self):

        localctx = ZCodeParser.DeclaredStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_declaredStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 204
            self.variablesDeclared()
            self.state = 205
            self.ignore()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AssignmentStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ASSIGN(self):
            return self.getToken(ZCodeParser.ASSIGN, 0)

        def express(self):
            return self.getTypedRuleContext(ZCodeParser.ExpressContext,0)


        def ignore(self):
            return self.getTypedRuleContext(ZCodeParser.IgnoreContext,0)


        def ID(self):
            return self.getToken(ZCodeParser.ID, 0)

        def indexOperatorNew(self):
            return self.getTypedRuleContext(ZCodeParser.IndexOperatorNewContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_assignmentStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssignmentStatement" ):
                listener.enterAssignmentStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssignmentStatement" ):
                listener.exitAssignmentStatement(self)




    def assignmentStatement(self):

        localctx = ZCodeParser.AssignmentStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_assignmentStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 210
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,15,self._ctx)
            if la_ == 1:
                self.state = 207
                self.match(ZCodeParser.ID)
                pass

            elif la_ == 2:
                self.state = 208
                self.match(ZCodeParser.ID)
                self.state = 209
                self.indexOperatorNew()
                pass


            self.state = 212
            self.match(ZCodeParser.ASSIGN)
            self.state = 213
            self.express()
            self.state = 214
            self.ignore()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IndexOperatorNewContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LBRACK(self):
            return self.getToken(ZCodeParser.LBRACK, 0)

        def expressList(self):
            return self.getTypedRuleContext(ZCodeParser.ExpressListContext,0)


        def RBRACK(self):
            return self.getToken(ZCodeParser.RBRACK, 0)

        def getRuleIndex(self):
            return ZCodeParser.RULE_indexOperatorNew

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIndexOperatorNew" ):
                listener.enterIndexOperatorNew(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIndexOperatorNew" ):
                listener.exitIndexOperatorNew(self)




    def indexOperatorNew(self):

        localctx = ZCodeParser.IndexOperatorNewContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_indexOperatorNew)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 216
            self.match(ZCodeParser.LBRACK)
            self.state = 217
            self.expressList()
            self.state = 218
            self.match(ZCodeParser.RBRACK)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ConditionStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IF(self):
            return self.getToken(ZCodeParser.IF, 0)

        def conditionAndStatement(self):
            return self.getTypedRuleContext(ZCodeParser.ConditionAndStatementContext,0)


        def elseIfStatement(self):
            return self.getTypedRuleContext(ZCodeParser.ElseIfStatementContext,0)


        def elseStatement(self):
            return self.getTypedRuleContext(ZCodeParser.ElseStatementContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_conditionStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConditionStatement" ):
                listener.enterConditionStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConditionStatement" ):
                listener.exitConditionStatement(self)




    def conditionStatement(self):

        localctx = ZCodeParser.ConditionStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_conditionStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 220
            self.match(ZCodeParser.IF)
            self.state = 221
            self.conditionAndStatement()
            self.state = 224
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,16,self._ctx)
            if la_ == 1:
                self.state = 222
                self.elseIfStatement()
                pass

            elif la_ == 2:
                pass


            self.state = 228
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,17,self._ctx)
            if la_ == 1:
                self.state = 226
                self.elseStatement()
                pass

            elif la_ == 2:
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ElseIfStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ELIF(self):
            return self.getToken(ZCodeParser.ELIF, 0)

        def conditionAndStatement(self):
            return self.getTypedRuleContext(ZCodeParser.ConditionAndStatementContext,0)


        def elseIfStatement(self):
            return self.getTypedRuleContext(ZCodeParser.ElseIfStatementContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_elseIfStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterElseIfStatement" ):
                listener.enterElseIfStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitElseIfStatement" ):
                listener.exitElseIfStatement(self)




    def elseIfStatement(self):

        localctx = ZCodeParser.ElseIfStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_elseIfStatement)
        try:
            self.state = 236
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 230
                self.match(ZCodeParser.ELIF)
                self.state = 231
                self.conditionAndStatement()
                self.state = 232
                self.elseIfStatement()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 234
                self.match(ZCodeParser.ELIF)
                self.state = 235
                self.conditionAndStatement()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ElseStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ELSE(self):
            return self.getToken(ZCodeParser.ELSE, 0)

        def ignore(self):
            return self.getTypedRuleContext(ZCodeParser.IgnoreContext,0)


        def statement(self):
            return self.getTypedRuleContext(ZCodeParser.StatementContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_elseStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterElseStatement" ):
                listener.enterElseStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitElseStatement" ):
                listener.exitElseStatement(self)




    def elseStatement(self):

        localctx = ZCodeParser.ElseStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_elseStatement)
        try:
            self.state = 244
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,19,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 238
                self.match(ZCodeParser.ELSE)
                self.state = 239
                self.ignore()
                self.state = 240
                self.statement()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 242
                self.match(ZCodeParser.ELSE)
                self.state = 243
                self.statement()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ConditionAndStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def conditionExpress(self):
            return self.getTypedRuleContext(ZCodeParser.ConditionExpressContext,0)


        def ignore(self):
            return self.getTypedRuleContext(ZCodeParser.IgnoreContext,0)


        def statement(self):
            return self.getTypedRuleContext(ZCodeParser.StatementContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_conditionAndStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConditionAndStatement" ):
                listener.enterConditionAndStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConditionAndStatement" ):
                listener.exitConditionAndStatement(self)




    def conditionAndStatement(self):

        localctx = ZCodeParser.ConditionAndStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_conditionAndStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 246
            self.conditionExpress()
            self.state = 251
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [46]:
                self.state = 247
                self.ignore()
                self.state = 248
                self.statement()
                pass
            elif token in [3, 4, 5, 6, 7, 8, 10, 13, 14, 15, 18, 42]:
                self.state = 250
                self.statement()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ConditionExpressContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAREN(self):
            return self.getToken(ZCodeParser.LPAREN, 0)

        def express(self):
            return self.getTypedRuleContext(ZCodeParser.ExpressContext,0)


        def RPAREN(self):
            return self.getToken(ZCodeParser.RPAREN, 0)

        def getRuleIndex(self):
            return ZCodeParser.RULE_conditionExpress

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConditionExpress" ):
                listener.enterConditionExpress(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConditionExpress" ):
                listener.exitConditionExpress(self)




    def conditionExpress(self):

        localctx = ZCodeParser.ConditionExpressContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_conditionExpress)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 253
            self.match(ZCodeParser.LPAREN)
            self.state = 254
            self.express()
            self.state = 255
            self.match(ZCodeParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ForStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FOR(self):
            return self.getToken(ZCodeParser.FOR, 0)

        def ID(self):
            return self.getToken(ZCodeParser.ID, 0)

        def UNTIL(self):
            return self.getToken(ZCodeParser.UNTIL, 0)

        def express(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ZCodeParser.ExpressContext)
            else:
                return self.getTypedRuleContext(ZCodeParser.ExpressContext,i)


        def BY(self):
            return self.getToken(ZCodeParser.BY, 0)

        def statement(self):
            return self.getTypedRuleContext(ZCodeParser.StatementContext,0)


        def ignore(self):
            return self.getTypedRuleContext(ZCodeParser.IgnoreContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_forStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterForStatement" ):
                listener.enterForStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitForStatement" ):
                listener.exitForStatement(self)




    def forStatement(self):

        localctx = ZCodeParser.ForStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_forStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 257
            self.match(ZCodeParser.FOR)
            self.state = 258
            self.match(ZCodeParser.ID)
            self.state = 259
            self.match(ZCodeParser.UNTIL)
            self.state = 260
            self.express()
            self.state = 261
            self.match(ZCodeParser.BY)
            self.state = 262
            self.express()
            self.state = 267
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [3, 4, 5, 6, 7, 8, 10, 13, 14, 15, 18, 42]:
                self.state = 263
                self.statement()
                pass
            elif token in [46]:
                self.state = 264
                self.ignore()
                self.state = 265
                self.statement()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ContinueStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CONTINUE(self):
            return self.getToken(ZCodeParser.CONTINUE, 0)

        def ignore(self):
            return self.getTypedRuleContext(ZCodeParser.IgnoreContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_continueStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterContinueStatement" ):
                listener.enterContinueStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitContinueStatement" ):
                listener.exitContinueStatement(self)




    def continueStatement(self):

        localctx = ZCodeParser.ContinueStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_continueStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 269
            self.match(ZCodeParser.CONTINUE)
            self.state = 270
            self.ignore()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BreakStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BREAK(self):
            return self.getToken(ZCodeParser.BREAK, 0)

        def ignore(self):
            return self.getTypedRuleContext(ZCodeParser.IgnoreContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_breakStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBreakStatement" ):
                listener.enterBreakStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBreakStatement" ):
                listener.exitBreakStatement(self)




    def breakStatement(self):

        localctx = ZCodeParser.BreakStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_breakStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 272
            self.match(ZCodeParser.BREAK)
            self.state = 273
            self.ignore()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ReturnStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def RETURN(self):
            return self.getToken(ZCodeParser.RETURN, 0)

        def ignore(self):
            return self.getTypedRuleContext(ZCodeParser.IgnoreContext,0)


        def express(self):
            return self.getTypedRuleContext(ZCodeParser.ExpressContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_returnStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterReturnStatement" ):
                listener.enterReturnStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitReturnStatement" ):
                listener.exitReturnStatement(self)




    def returnStatement(self):

        localctx = ZCodeParser.ReturnStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_returnStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 275
            self.match(ZCodeParser.RETURN)
            self.state = 278
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [1, 2, 21, 36, 37, 39, 42, 44, 45]:
                self.state = 276
                self.express()
                pass
            elif token in [46]:
                pass
            else:
                raise NoViableAltException(self)

            self.state = 280
            self.ignore()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BlockStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BEGIN(self):
            return self.getToken(ZCodeParser.BEGIN, 0)

        def ignore(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ZCodeParser.IgnoreContext)
            else:
                return self.getTypedRuleContext(ZCodeParser.IgnoreContext,i)


        def END(self):
            return self.getToken(ZCodeParser.END, 0)

        def statementList(self):
            return self.getTypedRuleContext(ZCodeParser.StatementListContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_blockStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBlockStatement" ):
                listener.enterBlockStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBlockStatement" ):
                listener.exitBlockStatement(self)




    def blockStatement(self):

        localctx = ZCodeParser.BlockStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 58, self.RULE_blockStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 282
            self.match(ZCodeParser.BEGIN)
            self.state = 283
            self.ignore()
            self.state = 286
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,23,self._ctx)
            if la_ == 1:
                self.state = 284
                self.statementList()
                pass

            elif la_ == 2:
                pass


            self.state = 288
            self.match(ZCodeParser.END)
            self.state = 289
            self.ignore()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CallFunctionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(ZCodeParser.ID, 0)

        def LPAREN(self):
            return self.getToken(ZCodeParser.LPAREN, 0)

        def RPAREN(self):
            return self.getToken(ZCodeParser.RPAREN, 0)

        def expressList(self):
            return self.getTypedRuleContext(ZCodeParser.ExpressListContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_callFunction

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCallFunction" ):
                listener.enterCallFunction(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCallFunction" ):
                listener.exitCallFunction(self)




    def callFunction(self):

        localctx = ZCodeParser.CallFunctionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 60, self.RULE_callFunction)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 291
            self.match(ZCodeParser.ID)
            self.state = 292
            self.match(ZCodeParser.LPAREN)
            self.state = 295
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [1, 2, 21, 36, 37, 39, 42, 44, 45]:
                self.state = 293
                self.expressList()
                pass
            elif token in [38]:
                pass
            else:
                raise NoViableAltException(self)

            self.state = 297
            self.match(ZCodeParser.RPAREN)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CallStatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(ZCodeParser.ID, 0)

        def LPAREN(self):
            return self.getToken(ZCodeParser.LPAREN, 0)

        def RPAREN(self):
            return self.getToken(ZCodeParser.RPAREN, 0)

        def ignore(self):
            return self.getTypedRuleContext(ZCodeParser.IgnoreContext,0)


        def expressList(self):
            return self.getTypedRuleContext(ZCodeParser.ExpressListContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_callStatement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCallStatement" ):
                listener.enterCallStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCallStatement" ):
                listener.exitCallStatement(self)




    def callStatement(self):

        localctx = ZCodeParser.CallStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 62, self.RULE_callStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 299
            self.match(ZCodeParser.ID)
            self.state = 300
            self.match(ZCodeParser.LPAREN)
            self.state = 303
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [1, 2, 21, 36, 37, 39, 42, 44, 45]:
                self.state = 301
                self.expressList()
                pass
            elif token in [38]:
                pass
            else:
                raise NoViableAltException(self)

            self.state = 305
            self.match(ZCodeParser.RPAREN)
            self.state = 306
            self.ignore()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressListContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def express(self):
            return self.getTypedRuleContext(ZCodeParser.ExpressContext,0)


        def COMMA(self):
            return self.getToken(ZCodeParser.COMMA, 0)

        def expressList(self):
            return self.getTypedRuleContext(ZCodeParser.ExpressListContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_expressList

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpressList" ):
                listener.enterExpressList(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpressList" ):
                listener.exitExpressList(self)




    def expressList(self):

        localctx = ZCodeParser.ExpressListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 64, self.RULE_expressList)
        try:
            self.state = 313
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,26,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 308
                self.express()
                self.state = 309
                self.match(ZCodeParser.COMMA)
                self.state = 310
                self.expressList()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 312
                self.express()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def express1(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ZCodeParser.Express1Context)
            else:
                return self.getTypedRuleContext(ZCodeParser.Express1Context,i)


        def CONCAT(self):
            return self.getToken(ZCodeParser.CONCAT, 0)

        def getRuleIndex(self):
            return ZCodeParser.RULE_express

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpress" ):
                listener.enterExpress(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpress" ):
                listener.exitExpress(self)




    def express(self):

        localctx = ZCodeParser.ExpressContext(self, self._ctx, self.state)
        self.enterRule(localctx, 66, self.RULE_express)
        try:
            self.state = 320
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,27,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 315
                self.express1()
                self.state = 316
                self.match(ZCodeParser.CONCAT)
                self.state = 317
                self.express1()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 319
                self.express1()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Express1Context(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def express2(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ZCodeParser.Express2Context)
            else:
                return self.getTypedRuleContext(ZCodeParser.Express2Context,i)


        def EQ(self):
            return self.getToken(ZCodeParser.EQ, 0)

        def EQEQ(self):
            return self.getToken(ZCodeParser.EQEQ, 0)

        def NEQ(self):
            return self.getToken(ZCodeParser.NEQ, 0)

        def LT(self):
            return self.getToken(ZCodeParser.LT, 0)

        def GT(self):
            return self.getToken(ZCodeParser.GT, 0)

        def LTE(self):
            return self.getToken(ZCodeParser.LTE, 0)

        def GTE(self):
            return self.getToken(ZCodeParser.GTE, 0)

        def getRuleIndex(self):
            return ZCodeParser.RULE_express1

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpress1" ):
                listener.enterExpress1(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpress1" ):
                listener.exitExpress1(self)




    def express1(self):

        localctx = ZCodeParser.Express1Context(self, self._ctx, self.state)
        self.enterRule(localctx, 68, self.RULE_express1)
        self._la = 0 # Token type
        try:
            self.state = 327
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,28,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 322
                self.express2(0)
                self.state = 323
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 12784238592) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 324
                self.express2(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 326
                self.express2(0)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Express2Context(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def express3(self):
            return self.getTypedRuleContext(ZCodeParser.Express3Context,0)


        def express2(self):
            return self.getTypedRuleContext(ZCodeParser.Express2Context,0)


        def AND(self):
            return self.getToken(ZCodeParser.AND, 0)

        def OR(self):
            return self.getToken(ZCodeParser.OR, 0)

        def getRuleIndex(self):
            return ZCodeParser.RULE_express2

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpress2" ):
                listener.enterExpress2(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpress2" ):
                listener.exitExpress2(self)



    def express2(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = ZCodeParser.Express2Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 70
        self.enterRecursionRule(localctx, 70, self.RULE_express2, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 330
            self.express3(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 337
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,29,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = ZCodeParser.Express2Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_express2)
                    self.state = 332
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 333
                    _la = self._input.LA(1)
                    if not(_la==34 or _la==35):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 334
                    self.express3(0) 
                self.state = 339
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,29,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Express3Context(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def express4(self):
            return self.getTypedRuleContext(ZCodeParser.Express4Context,0)


        def express3(self):
            return self.getTypedRuleContext(ZCodeParser.Express3Context,0)


        def ADD(self):
            return self.getToken(ZCodeParser.ADD, 0)

        def SUB(self):
            return self.getToken(ZCodeParser.SUB, 0)

        def getRuleIndex(self):
            return ZCodeParser.RULE_express3

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpress3" ):
                listener.enterExpress3(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpress3" ):
                listener.exitExpress3(self)



    def express3(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = ZCodeParser.Express3Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 72
        self.enterRecursionRule(localctx, 72, self.RULE_express3, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 341
            self.express4(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 348
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,30,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = ZCodeParser.Express3Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_express3)
                    self.state = 343
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 344
                    _la = self._input.LA(1)
                    if not(_la==20 or _la==21):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 345
                    self.express4(0) 
                self.state = 350
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,30,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Express4Context(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def express5(self):
            return self.getTypedRuleContext(ZCodeParser.Express5Context,0)


        def express4(self):
            return self.getTypedRuleContext(ZCodeParser.Express4Context,0)


        def MUL(self):
            return self.getToken(ZCodeParser.MUL, 0)

        def DIV(self):
            return self.getToken(ZCodeParser.DIV, 0)

        def MOD(self):
            return self.getToken(ZCodeParser.MOD, 0)

        def getRuleIndex(self):
            return ZCodeParser.RULE_express4

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpress4" ):
                listener.enterExpress4(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpress4" ):
                listener.exitExpress4(self)



    def express4(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = ZCodeParser.Express4Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 74
        self.enterRecursionRule(localctx, 74, self.RULE_express4, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 352
            self.express5()
            self._ctx.stop = self._input.LT(-1)
            self.state = 359
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,31,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = ZCodeParser.Express4Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_express4)
                    self.state = 354
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 355
                    _la = self._input.LA(1)
                    if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 29360128) != 0)):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 356
                    self.express5() 
                self.state = 361
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,31,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Express5Context(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NOT(self):
            return self.getToken(ZCodeParser.NOT, 0)

        def express5(self):
            return self.getTypedRuleContext(ZCodeParser.Express5Context,0)


        def express6(self):
            return self.getTypedRuleContext(ZCodeParser.Express6Context,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_express5

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpress5" ):
                listener.enterExpress5(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpress5" ):
                listener.exitExpress5(self)




    def express5(self):

        localctx = ZCodeParser.Express5Context(self, self._ctx, self.state)
        self.enterRule(localctx, 76, self.RULE_express5)
        try:
            self.state = 365
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [36]:
                self.enterOuterAlt(localctx, 1)
                self.state = 362
                self.match(ZCodeParser.NOT)
                self.state = 363
                self.express5()
                pass
            elif token in [1, 2, 21, 37, 39, 42, 44, 45]:
                self.enterOuterAlt(localctx, 2)
                self.state = 364
                self.express6()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Express6Context(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SUB(self):
            return self.getToken(ZCodeParser.SUB, 0)

        def express6(self):
            return self.getTypedRuleContext(ZCodeParser.Express6Context,0)


        def express7(self):
            return self.getTypedRuleContext(ZCodeParser.Express7Context,0)


        def express8(self):
            return self.getTypedRuleContext(ZCodeParser.Express8Context,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_express6

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpress6" ):
                listener.enterExpress6(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpress6" ):
                listener.exitExpress6(self)




    def express6(self):

        localctx = ZCodeParser.Express6Context(self, self._ctx, self.state)
        self.enterRule(localctx, 78, self.RULE_express6)
        try:
            self.state = 371
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,33,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 367
                self.match(ZCodeParser.SUB)
                self.state = 368
                self.express6()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 369
                self.express7()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 370
                self.express8()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Express7Context(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(ZCodeParser.ID, 0)

        def callFunction(self):
            return self.getTypedRuleContext(ZCodeParser.CallFunctionContext,0)


        def LBRACK(self):
            return self.getToken(ZCodeParser.LBRACK, 0)

        def expressList(self):
            return self.getTypedRuleContext(ZCodeParser.ExpressListContext,0)


        def RBRACK(self):
            return self.getToken(ZCodeParser.RBRACK, 0)

        def getRuleIndex(self):
            return ZCodeParser.RULE_express7

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpress7" ):
                listener.enterExpress7(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpress7" ):
                listener.exitExpress7(self)




    def express7(self):

        localctx = ZCodeParser.Express7Context(self, self._ctx, self.state)
        self.enterRule(localctx, 80, self.RULE_express7)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 375
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,34,self._ctx)
            if la_ == 1:
                self.state = 373
                self.match(ZCodeParser.ID)
                pass

            elif la_ == 2:
                self.state = 374
                self.callFunction()
                pass


            self.state = 377
            self.match(ZCodeParser.LBRACK)
            self.state = 378
            self.expressList()
            self.state = 379
            self.match(ZCodeParser.RBRACK)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Express8Context(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBERLIT(self):
            return self.getToken(ZCodeParser.NUMBERLIT, 0)

        def STRINGLIT(self):
            return self.getToken(ZCodeParser.STRINGLIT, 0)

        def TRUE(self):
            return self.getToken(ZCodeParser.TRUE, 0)

        def FALSE(self):
            return self.getToken(ZCodeParser.FALSE, 0)

        def ID(self):
            return self.getToken(ZCodeParser.ID, 0)

        def arrayLiteral(self):
            return self.getTypedRuleContext(ZCodeParser.ArrayLiteralContext,0)


        def LPAREN(self):
            return self.getToken(ZCodeParser.LPAREN, 0)

        def express(self):
            return self.getTypedRuleContext(ZCodeParser.ExpressContext,0)


        def RPAREN(self):
            return self.getToken(ZCodeParser.RPAREN, 0)

        def callFunction(self):
            return self.getTypedRuleContext(ZCodeParser.CallFunctionContext,0)


        def getRuleIndex(self):
            return ZCodeParser.RULE_express8

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpress8" ):
                listener.enterExpress8(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpress8" ):
                listener.exitExpress8(self)




    def express8(self):

        localctx = ZCodeParser.Express8Context(self, self._ctx, self.state)
        self.enterRule(localctx, 82, self.RULE_express8)
        try:
            self.state = 392
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,35,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 381
                self.match(ZCodeParser.NUMBERLIT)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 382
                self.match(ZCodeParser.STRINGLIT)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 383
                self.match(ZCodeParser.TRUE)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 384
                self.match(ZCodeParser.FALSE)
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 385
                self.match(ZCodeParser.ID)
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 386
                self.arrayLiteral()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 387
                self.match(ZCodeParser.LPAREN)
                self.state = 388
                self.express()
                self.state = 389
                self.match(ZCodeParser.RPAREN)
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 391
                self.callFunction()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ArrayLiteralContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LBRACK(self):
            return self.getToken(ZCodeParser.LBRACK, 0)

        def expressList(self):
            return self.getTypedRuleContext(ZCodeParser.ExpressListContext,0)


        def RBRACK(self):
            return self.getToken(ZCodeParser.RBRACK, 0)

        def getRuleIndex(self):
            return ZCodeParser.RULE_arrayLiteral

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArrayLiteral" ):
                listener.enterArrayLiteral(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArrayLiteral" ):
                listener.exitArrayLiteral(self)




    def arrayLiteral(self):

        localctx = ZCodeParser.ArrayLiteralContext(self, self._ctx, self.state)
        self.enterRule(localctx, 84, self.RULE_arrayLiteral)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 394
            self.match(ZCodeParser.LBRACK)
            self.state = 395
            self.expressList()
            self.state = 396
            self.match(ZCodeParser.RBRACK)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IgnoreContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NEWLINE(self, i:int=None):
            if i is None:
                return self.getTokens(ZCodeParser.NEWLINE)
            else:
                return self.getToken(ZCodeParser.NEWLINE, i)

        def getRuleIndex(self):
            return ZCodeParser.RULE_ignore

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIgnore" ):
                listener.enterIgnore(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIgnore" ):
                listener.exitIgnore(self)




    def ignore(self):

        localctx = ZCodeParser.IgnoreContext(self, self._ctx, self.state)
        self.enterRule(localctx, 86, self.RULE_ignore)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 399 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 398
                self.match(ZCodeParser.NEWLINE)
                self.state = 401 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==46):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[35] = self.express2_sempred
        self._predicates[36] = self.express3_sempred
        self._predicates[37] = self.express4_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def express2_sempred(self, localctx:Express2Context, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

    def express3_sempred(self, localctx:Express3Context, predIndex:int):
            if predIndex == 1:
                return self.precpred(self._ctx, 2)
         

    def express4_sempred(self, localctx:Express4Context, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 2)
         




