import unittest
from TestUtils import TestAST
from main.zcode.utils.AST import *

class ASTGenSuite(unittest.TestCase):
	def test_301(self):
		input = '''
var xo <- - (p0())
bool DK[42.98,24.74] <- jbv[true, "o", true]
'''
		expect = '''Program([VarDecl(Id(xo), None, var, UnaryOp(-, CallExpr(Id(p0), []))), VarDecl(Id(DK), ArrayType([42.98, 24.74], BoolType), None, ArrayCell(Id(jbv), [BooleanLit(True), StringLit(o), BooleanLit(True)]))])'''
		self.assertTrue(TestAST.test(input, expect, 301))

	def test_302(self):
		input = '''
func bF (bool HSL)	return 39.77

func vb5 (string nq0Z, number qT04, number LYsV[1.77,13.15])	return

func idx (number bIl)	return false

number SbI
'''
		expect = '''Program([FuncDecl(Id(bF), [VarDecl(Id(HSL), BoolType, None, None)], Return(NumLit(39.77))), FuncDecl(Id(vb5), [VarDecl(Id(nq0Z), StringType, None, None), VarDecl(Id(qT04), NumberType, None, None), VarDecl(Id(LYsV), ArrayType([1.77, 13.15], NumberType), None, None)], Return()), FuncDecl(Id(idx), [VarDecl(Id(bIl), NumberType, None, None)], Return(BooleanLit(False))), VarDecl(Id(SbI), NumberType, None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 302))

	def test_303(self):
		input = '''
func jN ()
	begin
	end
func l3ob (number Tsm[43.12,42.78])	begin
		if (96.15)
		return
		else if ("tVwk")
		number zP56[69.92,72.67,29.44] <- false
		elif (57.44) I1["l", HQC, "hKTo"] <- true
		elif (YV6m) Mp2A(49.21)
		elif (71.4)
		bool At_7[98.33,57.78,64.61]
		else number RG
		string eB[16.34]
		if (true) begin
		end
		else break
	end

'''
		expect = '''Program([FuncDecl(Id(jN), [], Block([])), FuncDecl(Id(l3ob), [VarDecl(Id(Tsm), ArrayType([43.12, 42.78], NumberType), None, None)], Block([If((NumLit(96.15), Return()), [], If((StringLit(tVwk), VarDecl(Id(zP56), ArrayType([69.92, 72.67, 29.44], NumberType), None, BooleanLit(False))), [(NumLit(57.44), AssignStmt(ArrayCell(Id(I1), [StringLit(l), Id(HQC), StringLit(hKTo)]), BooleanLit(True))), (Id(YV6m), CallStmt(Id(Mp2A), [NumLit(49.21)])), (NumLit(71.4), VarDecl(Id(At_7), ArrayType([98.33, 57.78, 64.61], BoolType), None, None))], VarDecl(Id(RG), NumberType, None, None))), VarDecl(Id(eB), ArrayType([16.34], StringType), None, None), If((BooleanLit(True), Block([])), [], Break)]))])'''
		self.assertTrue(TestAST.test(input, expect, 303))

	def test_304(self):
		input = '''
number t17h[16.15]
bool EMu
'''
		expect = '''Program([VarDecl(Id(t17h), ArrayType([16.15], NumberType), None, None), VarDecl(Id(EMu), BoolType, None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 304))

	def test_305(self):
		input = '''
func d6K (string FNK, bool gR, bool TQBN)	return
'''
		expect = '''Program([FuncDecl(Id(d6K), [VarDecl(Id(FNK), StringType, None, None), VarDecl(Id(gR), BoolType, None, None), VarDecl(Id(TQBN), BoolType, None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 305))

	def test_306(self):
		input = '''
string GjV
func kcY ()	return
number r9u[23.22,10.29]
string f1t[21.71,79.06]
string G4
'''
		expect = '''Program([VarDecl(Id(GjV), StringType, None, None), FuncDecl(Id(kcY), [], Return()), VarDecl(Id(r9u), ArrayType([23.22, 10.29], NumberType), None, None), VarDecl(Id(f1t), ArrayType([21.71, 79.06], StringType), None, None), VarDecl(Id(G4), StringType, None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 306))

	def test_307(self):
		input = '''
func pce (number Iz, string cP[29.46])	return 46.86
func t0nL (string jFHE)
	begin
		begin
			number RgUJ[27.34,71.36,24.73] <- 51.08
			if (94.29)
			if (false) break
			elif ("dOrJp")
			return
			elif (lR) break
			elif ("FMI") Gh[UU, "X"] <- 5.56
			elif (false)
			Z1QK(94.04, "vQOvu", yZ8c)
			elif (false)
			break
			else continue
			elif (true)
			Nad["Vtvxz"] <- "JAV"
			for Y7og until hX by "YE"
				vZfn <- true
		end
		bool ERzG <- 44.0
		if (true) dynamic jzc <- oeZ
		elif (Yvd)
		M3(IBr, O7f2, sr)
		elif (false) begin
		end
		elif ("innrd")
		continue
	end
func O9C (bool ZsKO, string k4, bool xp2O[80.0])
	begin
		if (true)
		if (true) break
		elif (h1OS) continue
		elif ("jRA")
		return "By"
		else continue
		elif (g3Ga) return true
		elif ("nz")
		if (53.22) return true
		elif (FF) number TIE[88.33,4.84,27.57] <- "e"
		elif (44.68)
		var u1PQ <- rPk
		elif (jrBV)
		number ED[78.19,89.84,86.02] <- 43.14
		elif ("zHUn") return
		elif (jZIE) continue
		else begin
			for oQ until "Oej" by "u"
				return
			dynamic wL6F
			Xo2w("FEw", false)
		end
		bool QAO <- false
	end
string GAa[23.15,36.94] <- true
'''
		expect = '''Program([FuncDecl(Id(pce), [VarDecl(Id(Iz), NumberType, None, None), VarDecl(Id(cP), ArrayType([29.46], StringType), None, None)], Return(NumLit(46.86))), FuncDecl(Id(t0nL), [VarDecl(Id(jFHE), StringType, None, None)], Block([Block([VarDecl(Id(RgUJ), ArrayType([27.34, 71.36, 24.73], NumberType), None, NumLit(51.08)), If((NumLit(94.29), If((BooleanLit(False), Break), [(StringLit(dOrJp), Return()), (Id(lR), Break), (StringLit(FMI), AssignStmt(ArrayCell(Id(Gh), [Id(UU), StringLit(X)]), NumLit(5.56))), (BooleanLit(False), CallStmt(Id(Z1QK), [NumLit(94.04), StringLit(vQOvu), Id(yZ8c)])), (BooleanLit(False), Break)], Continue)), [(BooleanLit(True), AssignStmt(ArrayCell(Id(Nad), [StringLit(Vtvxz)]), StringLit(JAV)))], None), For(Id(Y7og), Id(hX), StringLit(YE), AssignStmt(Id(vZfn), BooleanLit(True)))]), VarDecl(Id(ERzG), BoolType, None, NumLit(44.0)), If((BooleanLit(True), VarDecl(Id(jzc), None, dynamic, Id(oeZ))), [(Id(Yvd), CallStmt(Id(M3), [Id(IBr), Id(O7f2), Id(sr)])), (BooleanLit(False), Block([])), (StringLit(innrd), Continue)], None)])), FuncDecl(Id(O9C), [VarDecl(Id(ZsKO), BoolType, None, None), VarDecl(Id(k4), StringType, None, None), VarDecl(Id(xp2O), ArrayType([80.0], BoolType), None, None)], Block([If((BooleanLit(True), If((BooleanLit(True), Break), [(Id(h1OS), Continue), (StringLit(jRA), Return(StringLit(By)))], Continue)), [(Id(g3Ga), Return(BooleanLit(True))), (StringLit(nz), If((NumLit(53.22), Return(BooleanLit(True))), [(Id(FF), VarDecl(Id(TIE), ArrayType([88.33, 4.84, 27.57], NumberType), None, StringLit(e))), (NumLit(44.68), VarDecl(Id(u1PQ), None, var, Id(rPk))), (Id(jrBV), VarDecl(Id(ED), ArrayType([78.19, 89.84, 86.02], NumberType), None, NumLit(43.14))), (StringLit(zHUn), Return()), (Id(jZIE), Continue)], Block([For(Id(oQ), StringLit(Oej), StringLit(u), Return()), VarDecl(Id(wL6F), None, dynamic, None), CallStmt(Id(Xo2w), [StringLit(FEw), BooleanLit(False)])])))], None), VarDecl(Id(QAO), BoolType, None, BooleanLit(False))])), VarDecl(Id(GAa), ArrayType([23.15, 36.94], StringType), None, BooleanLit(True))])'''
		self.assertTrue(TestAST.test(input, expect, 307))

	def test_308(self):
		input = '''
func aSr (string YJee[97.43,58.95,28.98])	return 99.91
bool TOo0
'''
		expect = '''Program([FuncDecl(Id(aSr), [VarDecl(Id(YJee), ArrayType([97.43, 58.95, 28.98], StringType), None, None)], Return(NumLit(99.91))), VarDecl(Id(TOo0), BoolType, None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 308))

	def test_309(self):
		input = '''
string Qhb[64.57,59.32]
string wLb[39.69,82.65,81.07]
'''
		expect = '''Program([VarDecl(Id(Qhb), ArrayType([64.57, 59.32], StringType), None, None), VarDecl(Id(wLb), ArrayType([39.69, 82.65, 81.07], StringType), None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 309))

	def test_310(self):
		input = '''
number hM[54.79]
func sB (string Hy[15.39,61.25,47.38], string vjC, string je)	return false
dynamic P1G
'''
		expect = '''Program([VarDecl(Id(hM), ArrayType([54.79], NumberType), None, None), FuncDecl(Id(sB), [VarDecl(Id(Hy), ArrayType([15.39, 61.25, 47.38], StringType), None, None), VarDecl(Id(vjC), StringType, None, None), VarDecl(Id(je), StringType, None, None)], Return(BooleanLit(False))), VarDecl(Id(P1G), None, dynamic, None)])'''
		self.assertTrue(TestAST.test(input, expect, 310))

	def test_311(self):
		input = '''
bool eE[46.45] <- AxH
string sYE[13.48,2.91] <- "zDwWu"
bool ryR[24.45,35.94] <- true
'''
		expect = '''Program([VarDecl(Id(eE), ArrayType([46.45], BoolType), None, Id(AxH)), VarDecl(Id(sYE), ArrayType([13.48, 2.91], StringType), None, StringLit(zDwWu)), VarDecl(Id(ryR), ArrayType([24.45, 35.94], BoolType), None, BooleanLit(True))])'''
		self.assertTrue(TestAST.test(input, expect, 311))

	def test_312(self):
		input = '''
func iJ (number Nm, string o2Z, string lF[39.68,96.03])
	return
'''
		expect = '''Program([FuncDecl(Id(iJ), [VarDecl(Id(Nm), NumberType, None, None), VarDecl(Id(o2Z), StringType, None, None), VarDecl(Id(lF), ArrayType([39.68, 96.03], StringType), None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 312))

	def test_313(self):
		input = '''
bool dI[27.61,19.65]
func Wh (bool JW)	return

'''
		expect = '''Program([VarDecl(Id(dI), ArrayType([27.61, 19.65], BoolType), None, None), FuncDecl(Id(Wh), [VarDecl(Id(JW), BoolType, None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 313))

	def test_314(self):
		input = '''
string tlXU[3.06,22.12,18.97] <- 5.85
func rVTj (number XSN[48.08,65.97], string waC7, number Ne3)	begin
		break
		begin
			QMsn <- pW
		end
	end
func j_ (number mxP[78.52,60.67])
	return

'''
		expect = '''Program([VarDecl(Id(tlXU), ArrayType([3.06, 22.12, 18.97], StringType), None, NumLit(5.85)), FuncDecl(Id(rVTj), [VarDecl(Id(XSN), ArrayType([48.08, 65.97], NumberType), None, None), VarDecl(Id(waC7), StringType, None, None), VarDecl(Id(Ne3), NumberType, None, None)], Block([Break, Block([AssignStmt(Id(QMsn), Id(pW))])])), FuncDecl(Id(j_), [VarDecl(Id(mxP), ArrayType([78.52, 60.67], NumberType), None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 314))

	def test_315(self):
		input = '''
func sa (bool m6iQ, bool xWo, bool jhk)
	return gFk

func fC (number Em, number BCUR[80.2,52.49])	return

func Yo ()
	return

number oNIG[17.58,11.07,15.09]
'''
		expect = '''Program([FuncDecl(Id(sa), [VarDecl(Id(m6iQ), BoolType, None, None), VarDecl(Id(xWo), BoolType, None, None), VarDecl(Id(jhk), BoolType, None, None)], Return(Id(gFk))), FuncDecl(Id(fC), [VarDecl(Id(Em), NumberType, None, None), VarDecl(Id(BCUR), ArrayType([80.2, 52.49], NumberType), None, None)], Return()), FuncDecl(Id(Yo), [], Return()), VarDecl(Id(oNIG), ArrayType([17.58, 11.07, 15.09], NumberType), None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 315))

	def test_316(self):
		input = '''
func WYw (string t2[38.63])
	return true

var WwQn <- 22.27
string GPU1 <- true
'''
		expect = '''Program([FuncDecl(Id(WYw), [VarDecl(Id(t2), ArrayType([38.63], StringType), None, None)], Return(BooleanLit(True))), VarDecl(Id(WwQn), None, var, NumLit(22.27)), VarDecl(Id(GPU1), StringType, None, BooleanLit(True))])'''
		self.assertTrue(TestAST.test(input, expect, 316))

	def test_317(self):
		input = '''
number xLH <- true
func Yp ()
	return ohov

'''
		expect = '''Program([VarDecl(Id(xLH), NumberType, None, BooleanLit(True)), FuncDecl(Id(Yp), [], Return(Id(ohov)))])'''
		self.assertTrue(TestAST.test(input, expect, 317))

	def test_318(self):
		input = '''
func KyX (bool zwl)	begin
		begin
		end
	end
string Om
'''
		expect = '''Program([FuncDecl(Id(KyX), [VarDecl(Id(zwl), BoolType, None, None)], Block([Block([])])), VarDecl(Id(Om), StringType, None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 318))

	def test_319(self):
		input = '''
func aI (string X6n)
	return

'''
		expect = '''Program([FuncDecl(Id(aI), [VarDecl(Id(X6n), StringType, None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 319))

	def test_320(self):
		input = '''
func GJv_ (string Uh[99.44,4.84], number eN, bool Cl)
	begin
		continue
	end

'''
		expect = '''Program([FuncDecl(Id(GJv_), [VarDecl(Id(Uh), ArrayType([99.44, 4.84], StringType), None, None), VarDecl(Id(eN), NumberType, None, None), VarDecl(Id(Cl), BoolType, None, None)], Block([Continue]))])'''
		self.assertTrue(TestAST.test(input, expect, 320))

	def test_321(self):
		input = '''
func AGt (string hh[94.44,72.13,63.29])	return true
func Jk ()
	return false
func sd ()	return

'''
		expect = '''Program([FuncDecl(Id(AGt), [VarDecl(Id(hh), ArrayType([94.44, 72.13, 63.29], StringType), None, None)], Return(BooleanLit(True))), FuncDecl(Id(Jk), [], Return(BooleanLit(False))), FuncDecl(Id(sd), [], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 321))

	def test_322(self):
		input = '''
number E_P[29.43]
var XXa <- jKJs
func ZF (bool JZ)
	return

func uiKz ()	begin
		return k0
	end
'''
		expect = '''Program([VarDecl(Id(E_P), ArrayType([29.43], NumberType), None, None), VarDecl(Id(XXa), None, var, Id(jKJs)), FuncDecl(Id(ZF), [VarDecl(Id(JZ), BoolType, None, None)], Return()), FuncDecl(Id(uiKz), [], Block([Return(Id(k0))]))])'''
		self.assertTrue(TestAST.test(input, expect, 322))

	def test_323(self):
		input = '''
bool Qw
string ueZ[37.13,90.88,87.98] <- yl
number yxgb[90.79]
string Thz[43.6,95.5] <- Qkpy
func R5 (string nBZe)	return 75.64
'''
		expect = '''Program([VarDecl(Id(Qw), BoolType, None, None), VarDecl(Id(ueZ), ArrayType([37.13, 90.88, 87.98], StringType), None, Id(yl)), VarDecl(Id(yxgb), ArrayType([90.79], NumberType), None, None), VarDecl(Id(Thz), ArrayType([43.6, 95.5], StringType), None, Id(Qkpy)), FuncDecl(Id(R5), [VarDecl(Id(nBZe), StringType, None, None)], Return(NumLit(75.64)))])'''
		self.assertTrue(TestAST.test(input, expect, 323))

	def test_324(self):
		input = '''
func Lvt (bool GU[98.27,74.7,66.79], string G8Ga, number BnTs)
	return

'''
		expect = '''Program([FuncDecl(Id(Lvt), [VarDecl(Id(GU), ArrayType([98.27, 74.7, 66.79], BoolType), None, None), VarDecl(Id(G8Ga), StringType, None, None), VarDecl(Id(BnTs), NumberType, None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 324))

	def test_325(self):
		input = '''
bool Fn
number OfaX[48.57]
func kF ()	return
func i_qi (number Cr[88.69,71.99])
	begin
		pm(LhzF)
		if (zyW)
		SSlc("Y")
		elif ("t")
		if ("eAYpG") BI8M()
		else if (true)
		umC <- "wl"
		elif (84.13) continue
		else zx("MJU", "yXSCC")
	end

func qR_ (number d_L, string haq)	return "ln"

'''
		expect = '''Program([VarDecl(Id(Fn), BoolType, None, None), VarDecl(Id(OfaX), ArrayType([48.57], NumberType), None, None), FuncDecl(Id(kF), [], Return()), FuncDecl(Id(i_qi), [VarDecl(Id(Cr), ArrayType([88.69, 71.99], NumberType), None, None)], Block([CallStmt(Id(pm), [Id(LhzF)]), If((Id(zyW), CallStmt(Id(SSlc), [StringLit(Y)])), [(StringLit(t), If((StringLit(eAYpG), CallStmt(Id(BI8M), [])), [], If((BooleanLit(True), AssignStmt(Id(umC), StringLit(wl))), [(NumLit(84.13), Continue)], CallStmt(Id(zx), [StringLit(MJU), StringLit(yXSCC)]))))], None)])), FuncDecl(Id(qR_), [VarDecl(Id(d_L), NumberType, None, None), VarDecl(Id(haq), StringType, None, None)], Return(StringLit(ln)))])'''
		self.assertTrue(TestAST.test(input, expect, 325))

	def test_326(self):
		input = '''
func H_jT (number K2[9.04,8.71,55.56], number IP[72.77,57.0,27.24])
	return YB
dynamic qC <- 68.06
number EZ4
string DWt[77.53,0.59]
'''
		expect = '''Program([FuncDecl(Id(H_jT), [VarDecl(Id(K2), ArrayType([9.04, 8.71, 55.56], NumberType), None, None), VarDecl(Id(IP), ArrayType([72.77, 57.0, 27.24], NumberType), None, None)], Return(Id(YB))), VarDecl(Id(qC), None, dynamic, NumLit(68.06)), VarDecl(Id(EZ4), NumberType, None, None), VarDecl(Id(DWt), ArrayType([77.53, 0.59], StringType), None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 326))

	def test_327(self):
		input = '''
func Lo1z (string LUKD)	begin
		begin
			begin
				begin
					for Gv until gd by Cs
						continue
				end
				continue
			end
		end
		ZzJq(false)
	end

func cQai (number oRfP[72.98,88.35,33.46])	return "X"

func stye (number n5xA, string JBMZ, string a8[33.83,94.92,79.89])
	begin
		break
	end

'''
		expect = '''Program([FuncDecl(Id(Lo1z), [VarDecl(Id(LUKD), StringType, None, None)], Block([Block([Block([Block([For(Id(Gv), Id(gd), Id(Cs), Continue)]), Continue])]), CallStmt(Id(ZzJq), [BooleanLit(False)])])), FuncDecl(Id(cQai), [VarDecl(Id(oRfP), ArrayType([72.98, 88.35, 33.46], NumberType), None, None)], Return(StringLit(X))), FuncDecl(Id(stye), [VarDecl(Id(n5xA), NumberType, None, None), VarDecl(Id(JBMZ), StringType, None, None), VarDecl(Id(a8), ArrayType([33.83, 94.92, 79.89], StringType), None, None)], Block([Break]))])'''
		self.assertTrue(TestAST.test(input, expect, 327))

	def test_328(self):
		input = '''
func v7BW (bool pu[34.21], number vz, bool VP[66.69,22.59])	begin
	end

'''
		expect = '''Program([FuncDecl(Id(v7BW), [VarDecl(Id(pu), ArrayType([34.21], BoolType), None, None), VarDecl(Id(vz), NumberType, None, None), VarDecl(Id(VP), ArrayType([66.69, 22.59], BoolType), None, None)], Block([]))])'''
		self.assertTrue(TestAST.test(input, expect, 328))

	def test_329(self):
		input = '''
func ZO2O (number wqA[58.61], string Eno3, string mq9[30.19,75.84])
	return

func ed (bool I49[81.45,3.97], number RS)
	begin
		continue
		qG5(23.79)
	end

bool eJz
'''
		expect = '''Program([FuncDecl(Id(ZO2O), [VarDecl(Id(wqA), ArrayType([58.61], NumberType), None, None), VarDecl(Id(Eno3), StringType, None, None), VarDecl(Id(mq9), ArrayType([30.19, 75.84], StringType), None, None)], Return()), FuncDecl(Id(ed), [VarDecl(Id(I49), ArrayType([81.45, 3.97], BoolType), None, None), VarDecl(Id(RS), NumberType, None, None)], Block([Continue, CallStmt(Id(qG5), [NumLit(23.79)])])), VarDecl(Id(eJz), BoolType, None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 329))

	def test_330(self):
		input = '''
func wKNI ()	return false

number vC6[90.5] <- 86.57
bool pC[62.55,2.38,54.79]
'''
		expect = '''Program([FuncDecl(Id(wKNI), [], Return(BooleanLit(False))), VarDecl(Id(vC6), ArrayType([90.5], NumberType), None, NumLit(86.57)), VarDecl(Id(pC), ArrayType([62.55, 2.38, 54.79], BoolType), None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 330))

	def test_331(self):
		input = '''
func WO ()	return 72.99
string nyNL[18.2,58.2,69.89]
'''
		expect = '''Program([FuncDecl(Id(WO), [], Return(NumLit(72.99))), VarDecl(Id(nyNL), ArrayType([18.2, 58.2, 69.89], StringType), None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 331))

	def test_332(self):
		input = '''
func SS36 (string W3k[42.43,33.65,45.54], number Jx)
	begin
		continue
	end
bool Jv
'''
		expect = '''Program([FuncDecl(Id(SS36), [VarDecl(Id(W3k), ArrayType([42.43, 33.65, 45.54], StringType), None, None), VarDecl(Id(Jx), NumberType, None, None)], Block([Continue])), VarDecl(Id(Jv), BoolType, None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 332))

	def test_333(self):
		input = '''
func Ct (bool oKb[18.04], number VwdI)	return
string ZC[31.32]
func Al6o (bool QZmU[45.77])	return 34.71
dynamic xr
'''
		expect = '''Program([FuncDecl(Id(Ct), [VarDecl(Id(oKb), ArrayType([18.04], BoolType), None, None), VarDecl(Id(VwdI), NumberType, None, None)], Return()), VarDecl(Id(ZC), ArrayType([31.32], StringType), None, None), FuncDecl(Id(Al6o), [VarDecl(Id(QZmU), ArrayType([45.77], BoolType), None, None)], Return(NumLit(34.71))), VarDecl(Id(xr), None, dynamic, None)])'''
		self.assertTrue(TestAST.test(input, expect, 333))

	def test_334(self):
		input = '''
func G4 (bool LHnL)
	return

func Lbc ()
	return

func gq (number Dz, bool G9[33.34])
	return "G"
func sPm (number bi, string Z_[68.8], string O_PT)	return

'''
		expect = '''Program([FuncDecl(Id(G4), [VarDecl(Id(LHnL), BoolType, None, None)], Return()), FuncDecl(Id(Lbc), [], Return()), FuncDecl(Id(gq), [VarDecl(Id(Dz), NumberType, None, None), VarDecl(Id(G9), ArrayType([33.34], BoolType), None, None)], Return(StringLit(G))), FuncDecl(Id(sPm), [VarDecl(Id(bi), NumberType, None, None), VarDecl(Id(Z_), ArrayType([68.8], StringType), None, None), VarDecl(Id(O_PT), StringType, None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 334))

	def test_335(self):
		input = '''
bool GQ[25.57,27.34] <- KV
dynamic DY
func IU (bool fXhi, bool Oz[93.59,36.17])
	begin
		continue
		continue
	end

func mGs (string td[94.91,63.08], bool TM, bool R9f[26.74,41.26,82.68])
	return

'''
		expect = '''Program([VarDecl(Id(GQ), ArrayType([25.57, 27.34], BoolType), None, Id(KV)), VarDecl(Id(DY), None, dynamic, None), FuncDecl(Id(IU), [VarDecl(Id(fXhi), BoolType, None, None), VarDecl(Id(Oz), ArrayType([93.59, 36.17], BoolType), None, None)], Block([Continue, Continue])), FuncDecl(Id(mGs), [VarDecl(Id(td), ArrayType([94.91, 63.08], StringType), None, None), VarDecl(Id(TM), BoolType, None, None), VarDecl(Id(R9f), ArrayType([26.74, 41.26, 82.68], BoolType), None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 335))

	def test_336(self):
		input = '''
func GN (bool GeO[65.64,93.46])	return

'''
		expect = '''Program([FuncDecl(Id(GN), [VarDecl(Id(GeO), ArrayType([65.64, 93.46], BoolType), None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 336))

	def test_337(self):
		input = '''
func Kn (string d4Og, number CS)	begin
		dynamic Os2o
		begin
			continue
		end
		if ("sFU") continue
		elif (4.43)
		continue
		elif (75.38) IQmM()
		else break
	end

'''
		expect = '''Program([FuncDecl(Id(Kn), [VarDecl(Id(d4Og), StringType, None, None), VarDecl(Id(CS), NumberType, None, None)], Block([VarDecl(Id(Os2o), None, dynamic, None), Block([Continue]), If((StringLit(sFU), Continue), [(NumLit(4.43), Continue), (NumLit(75.38), CallStmt(Id(IQmM), []))], Break)]))])'''
		self.assertTrue(TestAST.test(input, expect, 337))

	def test_338(self):
		input = '''
var I_ <- 41.85
number RE
'''
		expect = '''Program([VarDecl(Id(I_), None, var, NumLit(41.85)), VarDecl(Id(RE), NumberType, None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 338))

	def test_339(self):
		input = '''
func limi (string ZD)	return

func iJ (number yZ, number gV8[72.74,45.39])
	begin
		rgH <- false
		for Qq until gt by 0.79
			break
		break
	end

func b6 (string Et, string Cam)	begin
		vo("U", false, vT)
		return true
	end
bool Ij6x
'''
		expect = '''Program([FuncDecl(Id(limi), [VarDecl(Id(ZD), StringType, None, None)], Return()), FuncDecl(Id(iJ), [VarDecl(Id(yZ), NumberType, None, None), VarDecl(Id(gV8), ArrayType([72.74, 45.39], NumberType), None, None)], Block([AssignStmt(Id(rgH), BooleanLit(False)), For(Id(Qq), Id(gt), NumLit(0.79), Break), Break])), FuncDecl(Id(b6), [VarDecl(Id(Et), StringType, None, None), VarDecl(Id(Cam), StringType, None, None)], Block([CallStmt(Id(vo), [StringLit(U), BooleanLit(False), Id(vT)]), Return(BooleanLit(True))])), VarDecl(Id(Ij6x), BoolType, None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 339))

	def test_340(self):
		input = '''
bool vPe[40.85,1.63]
'''
		expect = '''Program([VarDecl(Id(vPe), ArrayType([40.85, 1.63], BoolType), None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 340))

	def test_341(self):
		input = '''
func ov9 ()
	return
'''
		expect = '''Program([FuncDecl(Id(ov9), [], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 341))

	def test_342(self):
		input = '''
string GcZC[32.74,49.27,59.68]
func dpx ()	begin
		for BaqT until true by "KG"
			break
		return
	end
'''
		expect = '''Program([VarDecl(Id(GcZC), ArrayType([32.74, 49.27, 59.68], StringType), None, None), FuncDecl(Id(dpx), [], Block([For(Id(BaqT), BooleanLit(True), StringLit(KG), Break), Return()]))])'''
		self.assertTrue(TestAST.test(input, expect, 342))

	def test_343(self):
		input = '''
string GYz[58.02,36.39,47.31] <- 56.09
number ehf[48.45,27.57,49.07] <- true
func Oz0c (string n_WB[0.24])	begin
		return
	end

'''
		expect = '''Program([VarDecl(Id(GYz), ArrayType([58.02, 36.39, 47.31], StringType), None, NumLit(56.09)), VarDecl(Id(ehf), ArrayType([48.45, 27.57, 49.07], NumberType), None, BooleanLit(True)), FuncDecl(Id(Oz0c), [VarDecl(Id(n_WB), ArrayType([0.24], StringType), None, None)], Block([Return()]))])'''
		self.assertTrue(TestAST.test(input, expect, 343))

	def test_344(self):
		input = '''
dynamic bVP5 <- JHd
func F_6 (number UeW[79.87], bool d7[65.12,52.87], bool dpX)
	return

bool oj[21.67]
string EG[20.2] <- "NR"
func Fpt (number HKF[91.85,21.58,38.77], bool QYZ[38.9])	begin
	end
'''
		expect = '''Program([VarDecl(Id(bVP5), None, dynamic, Id(JHd)), FuncDecl(Id(F_6), [VarDecl(Id(UeW), ArrayType([79.87], NumberType), None, None), VarDecl(Id(d7), ArrayType([65.12, 52.87], BoolType), None, None), VarDecl(Id(dpX), BoolType, None, None)], Return()), VarDecl(Id(oj), ArrayType([21.67], BoolType), None, None), VarDecl(Id(EG), ArrayType([20.2], StringType), None, StringLit(NR)), FuncDecl(Id(Fpt), [VarDecl(Id(HKF), ArrayType([91.85, 21.58, 38.77], NumberType), None, None), VarDecl(Id(QYZ), ArrayType([38.9], BoolType), None, None)], Block([]))])'''
		self.assertTrue(TestAST.test(input, expect, 344))

	def test_345(self):
		input = '''
var PY <- 89.36
string JigW[59.22,8.45,44.57] <- "BccS"
func ao (bool rw, number QO[39.09,58.63], string iKOI[69.83])	return true

var Yt <- "KH"
func c6 (bool DuR, number EwmF, string u5_[80.97,94.69])
	return

'''
		expect = '''Program([VarDecl(Id(PY), None, var, NumLit(89.36)), VarDecl(Id(JigW), ArrayType([59.22, 8.45, 44.57], StringType), None, StringLit(BccS)), FuncDecl(Id(ao), [VarDecl(Id(rw), BoolType, None, None), VarDecl(Id(QO), ArrayType([39.09, 58.63], NumberType), None, None), VarDecl(Id(iKOI), ArrayType([69.83], StringType), None, None)], Return(BooleanLit(True))), VarDecl(Id(Yt), None, var, StringLit(KH)), FuncDecl(Id(c6), [VarDecl(Id(DuR), BoolType, None, None), VarDecl(Id(EwmF), NumberType, None, None), VarDecl(Id(u5_), ArrayType([80.97, 94.69], StringType), None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 345))

	def test_346(self):
		input = '''
func xh (bool SO4P, string YSe, bool Zy)	return "SEtS"
var FvS3 <- 32.74
func rYY8 ()
	begin
	end
dynamic Ix
number mh_[6.07,91.41]
'''
		expect = '''Program([FuncDecl(Id(xh), [VarDecl(Id(SO4P), BoolType, None, None), VarDecl(Id(YSe), StringType, None, None), VarDecl(Id(Zy), BoolType, None, None)], Return(StringLit(SEtS))), VarDecl(Id(FvS3), None, var, NumLit(32.74)), FuncDecl(Id(rYY8), [], Block([])), VarDecl(Id(Ix), None, dynamic, None), VarDecl(Id(mh_), ArrayType([6.07, 91.41], NumberType), None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 346))

	def test_347(self):
		input = '''
var WE <- "xQFZB"
func vkx (bool IQ[74.06])	begin
		if (nxG)
		begin
			bool AuI
			begin
			end
			return z4D
		end
		elif (RT6)
		break
		elif ("T")
		oF9 <- false
		elif (false) break
		elif (pZ) begin
			if (84.39)
			TL[true, 98.89] <- SK
			elif (true) if (60.01)
			HJ <- false
			elif (ieqH)
			continue
			elif (45.84) cB()
			elif ("RwtaU") continue
			elif ("UIoH")
			number OxWX[41.42]
			elif (true)
			for GPjI until 3.87 by "V"
				return 89.01
			elif (92.29) return
			else FDZp(52.31)
		end
		elif (false)
		begin
			return true
		end
	end
func HK (string fIjA, string Ghu)
	return "WjqJL"

'''
		expect = '''Program([VarDecl(Id(WE), None, var, StringLit(xQFZB)), FuncDecl(Id(vkx), [VarDecl(Id(IQ), ArrayType([74.06], BoolType), None, None)], Block([If((Id(nxG), Block([VarDecl(Id(AuI), BoolType, None, None), Block([]), Return(Id(z4D))])), [(Id(RT6), Break), (StringLit(T), AssignStmt(Id(oF9), BooleanLit(False))), (BooleanLit(False), Break), (Id(pZ), Block([If((NumLit(84.39), AssignStmt(ArrayCell(Id(TL), [BooleanLit(True), NumLit(98.89)]), Id(SK))), [(BooleanLit(True), If((NumLit(60.01), AssignStmt(Id(HJ), BooleanLit(False))), [(Id(ieqH), Continue), (NumLit(45.84), CallStmt(Id(cB), [])), (StringLit(RwtaU), Continue), (StringLit(UIoH), VarDecl(Id(OxWX), ArrayType([41.42], NumberType), None, None)), (BooleanLit(True), For(Id(GPjI), NumLit(3.87), StringLit(V), Return(NumLit(89.01)))), (NumLit(92.29), Return())], CallStmt(Id(FDZp), [NumLit(52.31)])))], None)])), (BooleanLit(False), Block([Return(BooleanLit(True))]))], None)])), FuncDecl(Id(HK), [VarDecl(Id(fIjA), StringType, None, None), VarDecl(Id(Ghu), StringType, None, None)], Return(StringLit(WjqJL)))])'''
		self.assertTrue(TestAST.test(input, expect, 347))

	def test_348(self):
		input = '''
func xvQ (bool lJ1s)	return
bool aZ5V
func HAe4 (string xdt4, number RVJ)	return true

'''
		expect = '''Program([FuncDecl(Id(xvQ), [VarDecl(Id(lJ1s), BoolType, None, None)], Return()), VarDecl(Id(aZ5V), BoolType, None, None), FuncDecl(Id(HAe4), [VarDecl(Id(xdt4), StringType, None, None), VarDecl(Id(RVJ), NumberType, None, None)], Return(BooleanLit(True)))])'''
		self.assertTrue(TestAST.test(input, expect, 348))

	def test_349(self):
		input = '''
func cQV ()	return "ETU"

number dX[62.32]
'''
		expect = '''Program([FuncDecl(Id(cQV), [], Return(StringLit(ETU))), VarDecl(Id(dX), ArrayType([62.32], NumberType), None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 349))

	def test_350(self):
		input = '''
bool sLr[92.79,97.3,81.48] <- "VHX"
bool ZW[43.08] <- true
number p9b[37.69,77.84,81.81]
func dR (bool tkh[21.54,92.4,59.14], bool df)	return
'''
		expect = '''Program([VarDecl(Id(sLr), ArrayType([92.79, 97.3, 81.48], BoolType), None, StringLit(VHX)), VarDecl(Id(ZW), ArrayType([43.08], BoolType), None, BooleanLit(True)), VarDecl(Id(p9b), ArrayType([37.69, 77.84, 81.81], NumberType), None, None), FuncDecl(Id(dR), [VarDecl(Id(tkh), ArrayType([21.54, 92.4, 59.14], BoolType), None, None), VarDecl(Id(df), BoolType, None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 350))

	def test_351(self):
		input = '''
number meb[66.01]
func Zt_o (number MF[82.38], number Sw[29.4], bool We5_)
	return
'''
		expect = '''Program([VarDecl(Id(meb), ArrayType([66.01], NumberType), None, None), FuncDecl(Id(Zt_o), [VarDecl(Id(MF), ArrayType([82.38], NumberType), None, None), VarDecl(Id(Sw), ArrayType([29.4], NumberType), None, None), VarDecl(Id(We5_), BoolType, None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 351))

	def test_352(self):
		input = '''
string Tobz
func Y8i ()
	return dc
'''
		expect = '''Program([VarDecl(Id(Tobz), StringType, None, None), FuncDecl(Id(Y8i), [], Return(Id(dc)))])'''
		self.assertTrue(TestAST.test(input, expect, 352))

	def test_353(self):
		input = '''
string BNN0[32.52,11.33]
func VZ ()	return

'''
		expect = '''Program([VarDecl(Id(BNN0), ArrayType([32.52, 11.33], StringType), None, None), FuncDecl(Id(VZ), [], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 353))

	def test_354(self):
		input = '''
func HlAb (bool T5, bool VFVO, number v2[68.88,61.5])	return false

bool lrV
func LVh (string o8[25.73,18.11], number tlm)
	return 93.7

'''
		expect = '''Program([FuncDecl(Id(HlAb), [VarDecl(Id(T5), BoolType, None, None), VarDecl(Id(VFVO), BoolType, None, None), VarDecl(Id(v2), ArrayType([68.88, 61.5], NumberType), None, None)], Return(BooleanLit(False))), VarDecl(Id(lrV), BoolType, None, None), FuncDecl(Id(LVh), [VarDecl(Id(o8), ArrayType([25.73, 18.11], StringType), None, None), VarDecl(Id(tlm), NumberType, None, None)], Return(NumLit(93.7)))])'''
		self.assertTrue(TestAST.test(input, expect, 354))

	def test_355(self):
		input = '''
func G9 (string yfdk[7.18,80.18], bool kxVG)	return "jsjx"
bool ft[29.33,18.74]
func XCUe (bool mrR1, bool izn[60.56], number cX[56.67,70.08])	return true

string METr
'''
		expect = '''Program([FuncDecl(Id(G9), [VarDecl(Id(yfdk), ArrayType([7.18, 80.18], StringType), None, None), VarDecl(Id(kxVG), BoolType, None, None)], Return(StringLit(jsjx))), VarDecl(Id(ft), ArrayType([29.33, 18.74], BoolType), None, None), FuncDecl(Id(XCUe), [VarDecl(Id(mrR1), BoolType, None, None), VarDecl(Id(izn), ArrayType([60.56], BoolType), None, None), VarDecl(Id(cX), ArrayType([56.67, 70.08], NumberType), None, None)], Return(BooleanLit(True))), VarDecl(Id(METr), StringType, None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 355))

	def test_356(self):
		input = '''
func ki ()
	return SrM
number tP[80.48,88.69,41.69] <- false
'''
		expect = '''Program([FuncDecl(Id(ki), [], Return(Id(SrM))), VarDecl(Id(tP), ArrayType([80.48, 88.69, 41.69], NumberType), None, BooleanLit(False))])'''
		self.assertTrue(TestAST.test(input, expect, 356))

	def test_357(self):
		input = '''
bool ILE[24.05,75.53] <- false
bool Y63[8.96,96.37,31.32]
func IS1 (bool aOBE, bool eSR, bool ES6[69.23,55.87])
	return true

number VdA
'''
		expect = '''Program([VarDecl(Id(ILE), ArrayType([24.05, 75.53], BoolType), None, BooleanLit(False)), VarDecl(Id(Y63), ArrayType([8.96, 96.37, 31.32], BoolType), None, None), FuncDecl(Id(IS1), [VarDecl(Id(aOBE), BoolType, None, None), VarDecl(Id(eSR), BoolType, None, None), VarDecl(Id(ES6), ArrayType([69.23, 55.87], BoolType), None, None)], Return(BooleanLit(True))), VarDecl(Id(VdA), NumberType, None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 357))

	def test_358(self):
		input = '''
bool AmA[32.84,39.2]
func HgN (string p1sB, bool X98e[16.18], number vrQ[53.86])	begin
		continue
	end
'''
		expect = '''Program([VarDecl(Id(AmA), ArrayType([32.84, 39.2], BoolType), None, None), FuncDecl(Id(HgN), [VarDecl(Id(p1sB), StringType, None, None), VarDecl(Id(X98e), ArrayType([16.18], BoolType), None, None), VarDecl(Id(vrQ), ArrayType([53.86], NumberType), None, None)], Block([Continue]))])'''
		self.assertTrue(TestAST.test(input, expect, 358))

	def test_359(self):
		input = '''
func P_ ()	return YG7
func WGGe (number cV, bool pQw2, number oFw3[82.55])	return JaP
'''
		expect = '''Program([FuncDecl(Id(P_), [], Return(Id(YG7))), FuncDecl(Id(WGGe), [VarDecl(Id(cV), NumberType, None, None), VarDecl(Id(pQw2), BoolType, None, None), VarDecl(Id(oFw3), ArrayType([82.55], NumberType), None, None)], Return(Id(JaP)))])'''
		self.assertTrue(TestAST.test(input, expect, 359))

	def test_360(self):
		input = '''
var X8tb <- "Dhe"
func ba (bool rGS[37.06])	begin
		begin
			n7(34.31)
			if ("fyP")
			continue
			elif (false) tykH["Jjtn"] <- ikG
			else number Uj <- 82.85
		end
		if (38.15) begin
			EmTe("wU")
		end
		if (true)
		return 88.98
		elif (31.65)
		fn(49.82, "bIK", n4d)
		else continue
	end
string kpBc[70.47,96.28] <- J0P0
string pui[55.63] <- 27.3
func mNH4 ()	return HK

'''
		expect = '''Program([VarDecl(Id(X8tb), None, var, StringLit(Dhe)), FuncDecl(Id(ba), [VarDecl(Id(rGS), ArrayType([37.06], BoolType), None, None)], Block([Block([CallStmt(Id(n7), [NumLit(34.31)]), If((StringLit(fyP), Continue), [(BooleanLit(False), AssignStmt(ArrayCell(Id(tykH), [StringLit(Jjtn)]), Id(ikG)))], VarDecl(Id(Uj), NumberType, None, NumLit(82.85)))]), If((NumLit(38.15), Block([CallStmt(Id(EmTe), [StringLit(wU)])])), [], None), If((BooleanLit(True), Return(NumLit(88.98))), [(NumLit(31.65), CallStmt(Id(fn), [NumLit(49.82), StringLit(bIK), Id(n4d)]))], Continue)])), VarDecl(Id(kpBc), ArrayType([70.47, 96.28], StringType), None, Id(J0P0)), VarDecl(Id(pui), ArrayType([55.63], StringType), None, NumLit(27.3)), FuncDecl(Id(mNH4), [], Return(Id(HK)))])'''
		self.assertTrue(TestAST.test(input, expect, 360))

	def test_361(self):
		input = '''
func HZGz ()
	return UXx

bool yO7I
func Nu (number C2CS[59.0,75.35,21.24])	begin
		for pY until RdA by "OXbka"
			return
		begin
			for WK until false by 42.72
				qHdn <- 51.97
			break
			if (39.11) number PR2y <- 7.96
			elif (false) if (8.88)
			lMcU["GInqh", 21.98] <- "fOMKP"
			elif (S5GG) break
			elif ("XVOkV") if (UW)
			for uMtt until "NWD" by 61.62
				number cYcm[68.66]
			elif (Uf)
			Ft()
			elif ("yFb")
			break
			elif ("kAzb")
			string qX71[33.19,17.27]
			elif ("H") return
			elif (gb8P) p_V[RRL, cZ3y, 76.59] <- "aRoni"
			elif (55.82) begin
				string v1yz[61.14,32.99] <- true
			end
			elif ("kOIZv") string M_pD[27.48,44.3,27.94]
			elif (KB4i)
			continue
		end
		if (By4) for dP until 5.24 by "hw"
			return false
	end
func c5 (number bU[21.86,42.94], number jivt[88.5])
	begin
		OyBJ <- Hj
		for Pp until true by EQ
			if (false) qit <- "KlppQ"
			elif (49.04)
			break
			elif (true) continue
	end
'''
		expect = '''Program([FuncDecl(Id(HZGz), [], Return(Id(UXx))), VarDecl(Id(yO7I), BoolType, None, None), FuncDecl(Id(Nu), [VarDecl(Id(C2CS), ArrayType([59.0, 75.35, 21.24], NumberType), None, None)], Block([For(Id(pY), Id(RdA), StringLit(OXbka), Return()), Block([For(Id(WK), BooleanLit(False), NumLit(42.72), AssignStmt(Id(qHdn), NumLit(51.97))), Break, If((NumLit(39.11), VarDecl(Id(PR2y), NumberType, None, NumLit(7.96))), [(BooleanLit(False), If((NumLit(8.88), AssignStmt(ArrayCell(Id(lMcU), [StringLit(GInqh), NumLit(21.98)]), StringLit(fOMKP))), [(Id(S5GG), Break), (StringLit(XVOkV), If((Id(UW), For(Id(uMtt), StringLit(NWD), NumLit(61.62), VarDecl(Id(cYcm), ArrayType([68.66], NumberType), None, None))), [(Id(Uf), CallStmt(Id(Ft), [])), (StringLit(yFb), Break), (StringLit(kAzb), VarDecl(Id(qX71), ArrayType([33.19, 17.27], StringType), None, None)), (StringLit(H), Return()), (Id(gb8P), AssignStmt(ArrayCell(Id(p_V), [Id(RRL), Id(cZ3y), NumLit(76.59)]), StringLit(aRoni))), (NumLit(55.82), Block([VarDecl(Id(v1yz), ArrayType([61.14, 32.99], StringType), None, BooleanLit(True))])), (StringLit(kOIZv), VarDecl(Id(M_pD), ArrayType([27.48, 44.3, 27.94], StringType), None, None)), (Id(KB4i), Continue)], None))], None))], None)]), If((Id(By4), For(Id(dP), NumLit(5.24), StringLit(hw), Return(BooleanLit(False)))), [], None)])), FuncDecl(Id(c5), [VarDecl(Id(bU), ArrayType([21.86, 42.94], NumberType), None, None), VarDecl(Id(jivt), ArrayType([88.5], NumberType), None, None)], Block([AssignStmt(Id(OyBJ), Id(Hj)), For(Id(Pp), BooleanLit(True), Id(EQ), If((BooleanLit(False), AssignStmt(Id(qit), StringLit(KlppQ))), [(NumLit(49.04), Break), (BooleanLit(True), Continue)], None))]))])'''
		self.assertTrue(TestAST.test(input, expect, 361))

	def test_362(self):
		input = '''
string tOE[44.13,78.11] <- "jdK"
func wFf9 (number y0[90.54,39.4], string c4Hk, string prPG)	return kvJT
'''
		expect = '''Program([VarDecl(Id(tOE), ArrayType([44.13, 78.11], StringType), None, StringLit(jdK)), FuncDecl(Id(wFf9), [VarDecl(Id(y0), ArrayType([90.54, 39.4], NumberType), None, None), VarDecl(Id(c4Hk), StringType, None, None), VarDecl(Id(prPG), StringType, None, None)], Return(Id(kvJT)))])'''
		self.assertTrue(TestAST.test(input, expect, 362))

	def test_363(self):
		input = '''
func ZQ (string T0Wj[28.58], string fe[52.62,37.66], number ODfk)	return 89.27
'''
		expect = '''Program([FuncDecl(Id(ZQ), [VarDecl(Id(T0Wj), ArrayType([28.58], StringType), None, None), VarDecl(Id(fe), ArrayType([52.62, 37.66], StringType), None, None), VarDecl(Id(ODfk), NumberType, None, None)], Return(NumLit(89.27)))])'''
		self.assertTrue(TestAST.test(input, expect, 363))

	def test_364(self):
		input = '''
func pW (string IRe[66.15,98.7], bool qOHr, bool TSy[36.96])	begin
		if (3.29) if (true) for nJ9 until 51.02 by mTBb
			if (true)
			continue
			elif (13.89) return
		elif ("i")
		break
		elif ("RH") begin
		end
		elif (13.26)
		break
		else continue
	end

string wpf
var y6pU <- hEv
number Qum[63.94] <- "k"
'''
		expect = '''Program([FuncDecl(Id(pW), [VarDecl(Id(IRe), ArrayType([66.15, 98.7], StringType), None, None), VarDecl(Id(qOHr), BoolType, None, None), VarDecl(Id(TSy), ArrayType([36.96], BoolType), None, None)], Block([If((NumLit(3.29), If((BooleanLit(True), For(Id(nJ9), NumLit(51.02), Id(mTBb), If((BooleanLit(True), Continue), [(NumLit(13.89), Return()), (StringLit(i), Break), (StringLit(RH), Block([])), (NumLit(13.26), Break)], Continue))), [], None)), [], None)])), VarDecl(Id(wpf), StringType, None, None), VarDecl(Id(y6pU), None, var, Id(hEv)), VarDecl(Id(Qum), ArrayType([63.94], NumberType), None, StringLit(k))])'''
		self.assertTrue(TestAST.test(input, expect, 364))

	def test_365(self):
		input = '''
number LJo <- "GaE"
func jw ()	return
bool ABs <- 56.26
'''
		expect = '''Program([VarDecl(Id(LJo), NumberType, None, StringLit(GaE)), FuncDecl(Id(jw), [], Return()), VarDecl(Id(ABs), BoolType, None, NumLit(56.26))])'''
		self.assertTrue(TestAST.test(input, expect, 365))

	def test_366(self):
		input = '''
dynamic GOs
func TEg8 (number A4KE[79.76,4.53])
	return

number Xu
string ZHy[16.93,86.89,23.37] <- false
'''
		expect = '''Program([VarDecl(Id(GOs), None, dynamic, None), FuncDecl(Id(TEg8), [VarDecl(Id(A4KE), ArrayType([79.76, 4.53], NumberType), None, None)], Return()), VarDecl(Id(Xu), NumberType, None, None), VarDecl(Id(ZHy), ArrayType([16.93, 86.89, 23.37], StringType), None, BooleanLit(False))])'''
		self.assertTrue(TestAST.test(input, expect, 366))

	def test_367(self):
		input = '''
func UAe4 (string gNh[11.74], number xHR[53.43])
	return

func GKkz (string n7SK[0.04,52.45,43.1])	begin
		begin
		end
		return Xd3J
		for Er until VCR by "M"
			break
	end
bool YOx[17.96,38.11,25.17]
'''
		expect = '''Program([FuncDecl(Id(UAe4), [VarDecl(Id(gNh), ArrayType([11.74], StringType), None, None), VarDecl(Id(xHR), ArrayType([53.43], NumberType), None, None)], Return()), FuncDecl(Id(GKkz), [VarDecl(Id(n7SK), ArrayType([0.04, 52.45, 43.1], StringType), None, None)], Block([Block([]), Return(Id(Xd3J)), For(Id(Er), Id(VCR), StringLit(M), Break)])), VarDecl(Id(YOx), ArrayType([17.96, 38.11, 25.17], BoolType), None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 367))

	def test_368(self):
		input = '''
var aDxG <- 40.34
'''
		expect = '''Program([VarDecl(Id(aDxG), None, var, NumLit(40.34))])'''
		self.assertTrue(TestAST.test(input, expect, 368))

	def test_369(self):
		input = '''
func ummw (bool jIg3)
	return "VzU"

var fb5V <- w4MW
var UzC0 <- "jG"
'''
		expect = '''Program([FuncDecl(Id(ummw), [VarDecl(Id(jIg3), BoolType, None, None)], Return(StringLit(VzU))), VarDecl(Id(fb5V), None, var, Id(w4MW)), VarDecl(Id(UzC0), None, var, StringLit(jG))])'''
		self.assertTrue(TestAST.test(input, expect, 369))

	def test_370(self):
		input = '''
func uOXc ()
	return true
'''
		expect = '''Program([FuncDecl(Id(uOXc), [], Return(BooleanLit(True)))])'''
		self.assertTrue(TestAST.test(input, expect, 370))

	def test_371(self):
		input = '''
func uE (string Bh[78.31,4.05], number PT[98.41,19.71,52.26], number gCY[61.62,21.15,48.67])
	return
number Cvu[66.81,24.7] <- ui
number w9S
func vxe (number aYi6, number XkP)	return Lx
number U1o[41.22,69.46,16.36]
'''
		expect = '''Program([FuncDecl(Id(uE), [VarDecl(Id(Bh), ArrayType([78.31, 4.05], StringType), None, None), VarDecl(Id(PT), ArrayType([98.41, 19.71, 52.26], NumberType), None, None), VarDecl(Id(gCY), ArrayType([61.62, 21.15, 48.67], NumberType), None, None)], Return()), VarDecl(Id(Cvu), ArrayType([66.81, 24.7], NumberType), None, Id(ui)), VarDecl(Id(w9S), NumberType, None, None), FuncDecl(Id(vxe), [VarDecl(Id(aYi6), NumberType, None, None), VarDecl(Id(XkP), NumberType, None, None)], Return(Id(Lx))), VarDecl(Id(U1o), ArrayType([41.22, 69.46, 16.36], NumberType), None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 371))

	def test_372(self):
		input = '''
string rH[38.33,13.63]
func rWiL (number Ab1, string wR)
	return 80.28

number Rim <- true
func wtR ()	return SS7

'''
		expect = '''Program([VarDecl(Id(rH), ArrayType([38.33, 13.63], StringType), None, None), FuncDecl(Id(rWiL), [VarDecl(Id(Ab1), NumberType, None, None), VarDecl(Id(wR), StringType, None, None)], Return(NumLit(80.28))), VarDecl(Id(Rim), NumberType, None, BooleanLit(True)), FuncDecl(Id(wtR), [], Return(Id(SS7)))])'''
		self.assertTrue(TestAST.test(input, expect, 372))

	def test_373(self):
		input = '''
func bO (number gL9[30.35], bool Pz7y)
	return

'''
		expect = '''Program([FuncDecl(Id(bO), [VarDecl(Id(gL9), ArrayType([30.35], NumberType), None, None), VarDecl(Id(Pz7y), BoolType, None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 373))

	def test_374(self):
		input = '''
string ZZy8 <- EJ
number YD[51.61] <- D4_p
number wUN[95.53]
func BTvG (number ZwSr, string wqH[45.08,9.6])
	return false
'''
		expect = '''Program([VarDecl(Id(ZZy8), StringType, None, Id(EJ)), VarDecl(Id(YD), ArrayType([51.61], NumberType), None, Id(D4_p)), VarDecl(Id(wUN), ArrayType([95.53], NumberType), None, None), FuncDecl(Id(BTvG), [VarDecl(Id(ZwSr), NumberType, None, None), VarDecl(Id(wqH), ArrayType([45.08, 9.6], StringType), None, None)], Return(BooleanLit(False)))])'''
		self.assertTrue(TestAST.test(input, expect, 374))

	def test_375(self):
		input = '''
func H3a (string UvUT, bool ld, bool BtJ)
	begin
		if ("Swb") if (33.31) if (cF0)
		for TAtl until "eFW" by "x"
			bool Xs5H <- U4B
		elif (Pynn) continue
		elif (66.51)
		begin
			continue
			for SJj until "AVz" by 60.11
				break
		end
		elif ("dzeDQ")
		bool WUxV
		else break
		elif (97.75)
		bool xg[14.51] <- "k"
		elif ("EOB") if (false) return true
		elif (98.5) continue
		elif (4.94)
		if (true)
		if (mg)
		string GBdh[71.34,18.31]
		elif (12.55)
		string tSZu
		elif (false) begin
			begin
				begin
					begin
						string UO <- q56
						dB0()
					end
				end
				JaIc(PeFu, O1, false)
				continue
			end
		end
		elif (ld2) return
		elif (HhA) break
		elif (38.48) continue
		else LVM("fm")
		elif (FOp) BU("PaGX", v0x)
		elif ("GSwR") begin
			Nje["WyICc"] <- 32.34
		end
		elif ("GASn")
		break
		elif ("U")
		return
		elif (fT7) cEBD(11.82, "Hji")
		elif (ol57) break
		elif (true) PH(dK, "p")
		elif ("yucJK") for N4C1 until JkoK by 6.28
			return
		elif ("S") break
		elif (true) rRSz <- "ZXZ"
		else break
		elif ("QMp")
		VBEL <- true
		elif (true) return
		elif ("OeMFy") for Pb8I until 67.76 by "QwJV"
			SkpB[p9z, true, "nG"] <- true
		elif (false) begin
		end
		else zX(45.03)
		dynamic oPQJ
		return
	end
func GM1m (number JDf[98.5])
	begin
		begin
			continue
			lU <- false
			VBan <- false
		end
		number wFhZ <- R1B2
		begin
			for b0 until true by FU_
				yyqt <- "LNoT"
			aJN()
			tR[false, "Er"] <- WOz
		end
	end
bool Lf1[66.52,14.83,82.87] <- BEX5
func Akk (number yds[49.93,9.79,39.89], number rO[50.65,17.53,82.7], string T54j)
	return "ZeCvH"

'''
		expect = '''Program([FuncDecl(Id(H3a), [VarDecl(Id(UvUT), StringType, None, None), VarDecl(Id(ld), BoolType, None, None), VarDecl(Id(BtJ), BoolType, None, None)], Block([If((StringLit(Swb), If((NumLit(33.31), If((Id(cF0), For(Id(TAtl), StringLit(eFW), StringLit(x), VarDecl(Id(Xs5H), BoolType, None, Id(U4B)))), [(Id(Pynn), Continue), (NumLit(66.51), Block([Continue, For(Id(SJj), StringLit(AVz), NumLit(60.11), Break)])), (StringLit(dzeDQ), VarDecl(Id(WUxV), BoolType, None, None))], Break)), [(NumLit(97.75), VarDecl(Id(xg), ArrayType([14.51], BoolType), None, StringLit(k))), (StringLit(EOB), If((BooleanLit(False), Return(BooleanLit(True))), [(NumLit(98.5), Continue), (NumLit(4.94), If((BooleanLit(True), If((Id(mg), VarDecl(Id(GBdh), ArrayType([71.34, 18.31], StringType), None, None)), [(NumLit(12.55), VarDecl(Id(tSZu), StringType, None, None)), (BooleanLit(False), Block([Block([Block([Block([VarDecl(Id(UO), StringType, None, Id(q56)), CallStmt(Id(dB0), [])])]), CallStmt(Id(JaIc), [Id(PeFu), Id(O1), BooleanLit(False)]), Continue])])), (Id(ld2), Return()), (Id(HhA), Break), (NumLit(38.48), Continue)], CallStmt(Id(LVM), [StringLit(fm)]))), [(Id(FOp), CallStmt(Id(BU), [StringLit(PaGX), Id(v0x)])), (StringLit(GSwR), Block([AssignStmt(ArrayCell(Id(Nje), [StringLit(WyICc)]), NumLit(32.34))])), (StringLit(GASn), Break), (StringLit(U), Return()), (Id(fT7), CallStmt(Id(cEBD), [NumLit(11.82), StringLit(Hji)])), (Id(ol57), Break), (BooleanLit(True), CallStmt(Id(PH), [Id(dK), StringLit(p)])), (StringLit(yucJK), For(Id(N4C1), Id(JkoK), NumLit(6.28), Return())), (StringLit(S), Break), (BooleanLit(True), AssignStmt(Id(rRSz), StringLit(ZXZ)))], Break)), (StringLit(QMp), AssignStmt(Id(VBEL), BooleanLit(True))), (BooleanLit(True), Return()), (StringLit(OeMFy), For(Id(Pb8I), NumLit(67.76), StringLit(QwJV), AssignStmt(ArrayCell(Id(SkpB), [Id(p9z), BooleanLit(True), StringLit(nG)]), BooleanLit(True)))), (BooleanLit(False), Block([]))], CallStmt(Id(zX), [NumLit(45.03)])))], None)), [], None), VarDecl(Id(oPQJ), None, dynamic, None), Return()])), FuncDecl(Id(GM1m), [VarDecl(Id(JDf), ArrayType([98.5], NumberType), None, None)], Block([Block([Continue, AssignStmt(Id(lU), BooleanLit(False)), AssignStmt(Id(VBan), BooleanLit(False))]), VarDecl(Id(wFhZ), NumberType, None, Id(R1B2)), Block([For(Id(b0), BooleanLit(True), Id(FU_), AssignStmt(Id(yyqt), StringLit(LNoT))), CallStmt(Id(aJN), []), AssignStmt(ArrayCell(Id(tR), [BooleanLit(False), StringLit(Er)]), Id(WOz))])])), VarDecl(Id(Lf1), ArrayType([66.52, 14.83, 82.87], BoolType), None, Id(BEX5)), FuncDecl(Id(Akk), [VarDecl(Id(yds), ArrayType([49.93, 9.79, 39.89], NumberType), None, None), VarDecl(Id(rO), ArrayType([50.65, 17.53, 82.7], NumberType), None, None), VarDecl(Id(T54j), StringType, None, None)], Return(StringLit(ZeCvH)))])'''
		self.assertTrue(TestAST.test(input, expect, 375))

	def test_376(self):
		input = '''
bool XJU[36.31,66.3] <- "f"
func pUL (bool V_[54.28,81.64,76.11], string xQ[87.97,21.13,26.65], string R9e)	return
'''
		expect = '''Program([VarDecl(Id(XJU), ArrayType([36.31, 66.3], BoolType), None, StringLit(f)), FuncDecl(Id(pUL), [VarDecl(Id(V_), ArrayType([54.28, 81.64, 76.11], BoolType), None, None), VarDecl(Id(xQ), ArrayType([87.97, 21.13, 26.65], StringType), None, None), VarDecl(Id(R9e), StringType, None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 376))

	def test_377(self):
		input = '''
func zbY7 (number OkPk, bool suS[43.41], bool OdR)
	return "wwJ"
'''
		expect = '''Program([FuncDecl(Id(zbY7), [VarDecl(Id(OkPk), NumberType, None, None), VarDecl(Id(suS), ArrayType([43.41], BoolType), None, None), VarDecl(Id(OdR), BoolType, None, None)], Return(StringLit(wwJ)))])'''
		self.assertTrue(TestAST.test(input, expect, 377))

	def test_378(self):
		input = '''
bool C2[28.97,86.7,63.24]
func d9 (string owTx, bool y4[4.12,25.46,89.13])
	return 80.25

func RO (number Dmc6[59.21], bool MMf0, bool sYL[37.42])
	return "hidd"
func ev (number Fu0E, number dx[25.4])
	begin
	end
'''
		expect = '''Program([VarDecl(Id(C2), ArrayType([28.97, 86.7, 63.24], BoolType), None, None), FuncDecl(Id(d9), [VarDecl(Id(owTx), StringType, None, None), VarDecl(Id(y4), ArrayType([4.12, 25.46, 89.13], BoolType), None, None)], Return(NumLit(80.25))), FuncDecl(Id(RO), [VarDecl(Id(Dmc6), ArrayType([59.21], NumberType), None, None), VarDecl(Id(MMf0), BoolType, None, None), VarDecl(Id(sYL), ArrayType([37.42], BoolType), None, None)], Return(StringLit(hidd))), FuncDecl(Id(ev), [VarDecl(Id(Fu0E), NumberType, None, None), VarDecl(Id(dx), ArrayType([25.4], NumberType), None, None)], Block([]))])'''
		self.assertTrue(TestAST.test(input, expect, 378))

	def test_379(self):
		input = '''
func sdGs ()
	return "R"
bool si[13.71,22.33,85.83] <- true
'''
		expect = '''Program([FuncDecl(Id(sdGs), [], Return(StringLit(R))), VarDecl(Id(si), ArrayType([13.71, 22.33, 85.83], BoolType), None, BooleanLit(True))])'''
		self.assertTrue(TestAST.test(input, expect, 379))

	def test_380(self):
		input = '''
number AEO[25.2,65.31]
func Yv4 ()
	begin
	end

'''
		expect = '''Program([VarDecl(Id(AEO), ArrayType([25.2, 65.31], NumberType), None, None), FuncDecl(Id(Yv4), [], Block([]))])'''
		self.assertTrue(TestAST.test(input, expect, 380))

	def test_381(self):
		input = '''
func zfQ1 (number Mw[30.72,64.79,74.44], number h7m, string DIon)
	return true

func wFq (string Ye[24.46], bool x39)	return "IABV"

func TRA ()	return OMV6

bool eWre <- h31
bool Og[35.92] <- kY
'''
		expect = '''Program([FuncDecl(Id(zfQ1), [VarDecl(Id(Mw), ArrayType([30.72, 64.79, 74.44], NumberType), None, None), VarDecl(Id(h7m), NumberType, None, None), VarDecl(Id(DIon), StringType, None, None)], Return(BooleanLit(True))), FuncDecl(Id(wFq), [VarDecl(Id(Ye), ArrayType([24.46], StringType), None, None), VarDecl(Id(x39), BoolType, None, None)], Return(StringLit(IABV))), FuncDecl(Id(TRA), [], Return(Id(OMV6))), VarDecl(Id(eWre), BoolType, None, Id(h31)), VarDecl(Id(Og), ArrayType([35.92], BoolType), None, Id(kY))])'''
		self.assertTrue(TestAST.test(input, expect, 381))

	def test_382(self):
		input = '''
func W3i (string HI, string b14)
	return "jrpUe"

bool xf[41.8,64.22,67.39]
func Y5Q ()
	return
func uBP (bool ZOF)	return ga
number PIX[29.39,10.07] <- 51.89
'''
		expect = '''Program([FuncDecl(Id(W3i), [VarDecl(Id(HI), StringType, None, None), VarDecl(Id(b14), StringType, None, None)], Return(StringLit(jrpUe))), VarDecl(Id(xf), ArrayType([41.8, 64.22, 67.39], BoolType), None, None), FuncDecl(Id(Y5Q), [], Return()), FuncDecl(Id(uBP), [VarDecl(Id(ZOF), BoolType, None, None)], Return(Id(ga))), VarDecl(Id(PIX), ArrayType([29.39, 10.07], NumberType), None, NumLit(51.89))])'''
		self.assertTrue(TestAST.test(input, expect, 382))

	def test_383(self):
		input = '''
func Wz (number V4Ps)
	return

func yEZJ (string KK, string KlKg, bool Gp[79.33])
	return true

number lhO <- NW
'''
		expect = '''Program([FuncDecl(Id(Wz), [VarDecl(Id(V4Ps), NumberType, None, None)], Return()), FuncDecl(Id(yEZJ), [VarDecl(Id(KK), StringType, None, None), VarDecl(Id(KlKg), StringType, None, None), VarDecl(Id(Gp), ArrayType([79.33], BoolType), None, None)], Return(BooleanLit(True))), VarDecl(Id(lhO), NumberType, None, Id(NW))])'''
		self.assertTrue(TestAST.test(input, expect, 383))

	def test_384(self):
		input = '''
bool P6[83.63,23.25]
string xp[47.5,45.89,71.33] <- 36.04
func VQ ()	return "jVJ"
'''
		expect = '''Program([VarDecl(Id(P6), ArrayType([83.63, 23.25], BoolType), None, None), VarDecl(Id(xp), ArrayType([47.5, 45.89, 71.33], StringType), None, NumLit(36.04)), FuncDecl(Id(VQ), [], Return(StringLit(jVJ)))])'''
		self.assertTrue(TestAST.test(input, expect, 384))

	def test_385(self):
		input = '''
func Qe ()	return 33.33

var C7 <- "IZzE"
'''
		expect = '''Program([FuncDecl(Id(Qe), [], Return(NumLit(33.33))), VarDecl(Id(C7), None, var, StringLit(IZzE))])'''
		self.assertTrue(TestAST.test(input, expect, 385))

	def test_386(self):
		input = '''
bool EH[78.38,32.08] <- 36.81
'''
		expect = '''Program([VarDecl(Id(EH), ArrayType([78.38, 32.08], BoolType), None, NumLit(36.81))])'''
		self.assertTrue(TestAST.test(input, expect, 386))

	def test_387(self):
		input = '''
func IgD (number IgG[89.82,94.47], bool R18)	begin
		continue
		begin
		end
	end
'''
		expect = '''Program([FuncDecl(Id(IgD), [VarDecl(Id(IgG), ArrayType([89.82, 94.47], NumberType), None, None), VarDecl(Id(R18), BoolType, None, None)], Block([Continue, Block([])]))])'''
		self.assertTrue(TestAST.test(input, expect, 387))

	def test_388(self):
		input = '''
func FN (bool atg, number tt, bool po)	return yl
number PY <- false
string br0[47.95]
func h5Pu ()	return true

'''
		expect = '''Program([FuncDecl(Id(FN), [VarDecl(Id(atg), BoolType, None, None), VarDecl(Id(tt), NumberType, None, None), VarDecl(Id(po), BoolType, None, None)], Return(Id(yl))), VarDecl(Id(PY), NumberType, None, BooleanLit(False)), VarDecl(Id(br0), ArrayType([47.95], StringType), None, None), FuncDecl(Id(h5Pu), [], Return(BooleanLit(True)))])'''
		self.assertTrue(TestAST.test(input, expect, 388))

	def test_389(self):
		input = '''
number nf[81.17,4.95,80.72] <- 76.93
dynamic Vc
string WuKE[33.17]
bool rax[1.45,62.52,57.32]
'''
		expect = '''Program([VarDecl(Id(nf), ArrayType([81.17, 4.95, 80.72], NumberType), None, NumLit(76.93)), VarDecl(Id(Vc), None, dynamic, None), VarDecl(Id(WuKE), ArrayType([33.17], StringType), None, None), VarDecl(Id(rax), ArrayType([1.45, 62.52, 57.32], BoolType), None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 389))

	def test_390(self):
		input = '''
bool JIA[30.26,94.0,98.21] <- T7am
bool oI4f[11.22,33.25] <- wf
func Rz ()
	begin
		return i7
		Ip(false, 86.84, "Rq")
	end
'''
		expect = '''Program([VarDecl(Id(JIA), ArrayType([30.26, 94.0, 98.21], BoolType), None, Id(T7am)), VarDecl(Id(oI4f), ArrayType([11.22, 33.25], BoolType), None, Id(wf)), FuncDecl(Id(Rz), [], Block([Return(Id(i7)), CallStmt(Id(Ip), [BooleanLit(False), NumLit(86.84), StringLit(Rq)])]))])'''
		self.assertTrue(TestAST.test(input, expect, 390))

	def test_391(self):
		input = '''
func C5o (bool Ws0, string DO)	begin
		for FKgp until tb by VnHY
			begin
				return
			end
		return
		return
	end
func Vif3 (bool ZPk_[7.31])	return G2c

func nqtB (bool Vg[36.41,51.76,95.16], bool TON[2.62], number yjoU)	return

'''
		expect = '''Program([FuncDecl(Id(C5o), [VarDecl(Id(Ws0), BoolType, None, None), VarDecl(Id(DO), StringType, None, None)], Block([For(Id(FKgp), Id(tb), Id(VnHY), Block([Return()])), Return(), Return()])), FuncDecl(Id(Vif3), [VarDecl(Id(ZPk_), ArrayType([7.31], BoolType), None, None)], Return(Id(G2c))), FuncDecl(Id(nqtB), [VarDecl(Id(Vg), ArrayType([36.41, 51.76, 95.16], BoolType), None, None), VarDecl(Id(TON), ArrayType([2.62], BoolType), None, None), VarDecl(Id(yjoU), NumberType, None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 391))

	def test_392(self):
		input = '''
func cR (string qqJ, string u9y[13.65,54.7])	return 78.73
func M8 ()
	return kbtj

string kP4[95.3,72.49]
'''
		expect = '''Program([FuncDecl(Id(cR), [VarDecl(Id(qqJ), StringType, None, None), VarDecl(Id(u9y), ArrayType([13.65, 54.7], StringType), None, None)], Return(NumLit(78.73))), FuncDecl(Id(M8), [], Return(Id(kbtj))), VarDecl(Id(kP4), ArrayType([95.3, 72.49], StringType), None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 392))

	def test_393(self):
		input = '''
func uQf ()	return
bool fUaL[34.17,81.64,28.25] <- 56.53
dynamic ML <- ss
bool lH[96.59,89.49,46.3]
'''
		expect = '''Program([FuncDecl(Id(uQf), [], Return()), VarDecl(Id(fUaL), ArrayType([34.17, 81.64, 28.25], BoolType), None, NumLit(56.53)), VarDecl(Id(ML), None, dynamic, Id(ss)), VarDecl(Id(lH), ArrayType([96.59, 89.49, 46.3], BoolType), None, None)])'''
		self.assertTrue(TestAST.test(input, expect, 393))

	def test_394(self):
		input = '''
string tay
func SC2 (bool Iud)
	begin
		begin
			break
			for rA9 until xN by UMN
				if (true) return 11.52
				elif (w5O) for Nm until 76.47 by 5.1
					if (false)
					continue
					elif (false) return
					else oVc[30.13, "KFQSX", 39.63] <- qM5k
				else vu(rH, "pmyJ")
		end
		return
	end
'''
		expect = '''Program([VarDecl(Id(tay), StringType, None, None), FuncDecl(Id(SC2), [VarDecl(Id(Iud), BoolType, None, None)], Block([Block([Break, For(Id(rA9), Id(xN), Id(UMN), If((BooleanLit(True), Return(NumLit(11.52))), [(Id(w5O), For(Id(Nm), NumLit(76.47), NumLit(5.1), If((BooleanLit(False), Continue), [(BooleanLit(False), Return())], AssignStmt(ArrayCell(Id(oVc), [NumLit(30.13), StringLit(KFQSX), NumLit(39.63)]), Id(qM5k)))))], CallStmt(Id(vu), [Id(rH), StringLit(pmyJ)])))]), Return()]))])'''
		self.assertTrue(TestAST.test(input, expect, 394))

	def test_395(self):
		input = '''
func WbZ (number s_[14.94], number Wsw[42.9,37.72,74.75], bool VQlR[74.51,20.2,92.39])
	return iL
'''
		expect = '''Program([FuncDecl(Id(WbZ), [VarDecl(Id(s_), ArrayType([14.94], NumberType), None, None), VarDecl(Id(Wsw), ArrayType([42.9, 37.72, 74.75], NumberType), None, None), VarDecl(Id(VQlR), ArrayType([74.51, 20.2, 92.39], BoolType), None, None)], Return(Id(iL)))])'''
		self.assertTrue(TestAST.test(input, expect, 395))

	def test_396(self):
		input = '''
dynamic vTC <- true
func ODx (number It0M, string OjT)	return

number aZN[6.74,1.81]
dynamic pcLE
'''
		expect = '''Program([VarDecl(Id(vTC), None, dynamic, BooleanLit(True)), FuncDecl(Id(ODx), [VarDecl(Id(It0M), NumberType, None, None), VarDecl(Id(OjT), StringType, None, None)], Return()), VarDecl(Id(aZN), ArrayType([6.74, 1.81], NumberType), None, None), VarDecl(Id(pcLE), None, dynamic, None)])'''
		self.assertTrue(TestAST.test(input, expect, 396))

	def test_397(self):
		input = '''
string GKsb <- 54.85
func FIyq (bool tKR[44.44], number i7El)	begin
		if (cG) bool vgI[54.33,54.6,17.71]
		elif (68.42) Obn <- 72.76
		elif ("I")
		continue
		elif ("MxqB") X16f <- 33.09
		elif (48.08)
		break
		elif (1.94) begin
			begin
				wR[94.59, xH, false] <- true
				begin
					if (94.32) continue
					elif (89.85)
					teKc("WpV", "PduqU")
				end
				string sM3G[31.9,2.42] <- 52.95
			end
			for NlA until 16.49 by 66.92
				begin
					bool ab[37.01]
				end
		end
		else vl()
		fo(false)
	end

'''
		expect = '''Program([VarDecl(Id(GKsb), StringType, None, NumLit(54.85)), FuncDecl(Id(FIyq), [VarDecl(Id(tKR), ArrayType([44.44], BoolType), None, None), VarDecl(Id(i7El), NumberType, None, None)], Block([If((Id(cG), VarDecl(Id(vgI), ArrayType([54.33, 54.6, 17.71], BoolType), None, None)), [(NumLit(68.42), AssignStmt(Id(Obn), NumLit(72.76))), (StringLit(I), Continue), (StringLit(MxqB), AssignStmt(Id(X16f), NumLit(33.09))), (NumLit(48.08), Break), (NumLit(1.94), Block([Block([AssignStmt(ArrayCell(Id(wR), [NumLit(94.59), Id(xH), BooleanLit(False)]), BooleanLit(True)), Block([If((NumLit(94.32), Continue), [(NumLit(89.85), CallStmt(Id(teKc), [StringLit(WpV), StringLit(PduqU)]))], None)]), VarDecl(Id(sM3G), ArrayType([31.9, 2.42], StringType), None, NumLit(52.95))]), For(Id(NlA), NumLit(16.49), NumLit(66.92), Block([VarDecl(Id(ab), ArrayType([37.01], BoolType), None, None)]))]))], CallStmt(Id(vl), [])), CallStmt(Id(fo), [BooleanLit(False)])]))])'''
		self.assertTrue(TestAST.test(input, expect, 397))

	def test_398(self):
		input = '''
func Oc (string bdVo, string dTp, number kz[64.02])	return mD9i
'''
		expect = '''Program([FuncDecl(Id(Oc), [VarDecl(Id(bdVo), StringType, None, None), VarDecl(Id(dTp), StringType, None, None), VarDecl(Id(kz), ArrayType([64.02], NumberType), None, None)], Return(Id(mD9i)))])'''
		self.assertTrue(TestAST.test(input, expect, 398))

	def test_399(self):
		input = '''
string Zz5[59.36,95.97,1.32]
func LBZ (bool oll6[7.0], string S9[51.84,68.06])
	return
func vzq (bool CHRl, bool Rw[47.13,52.54,96.0])
	return

'''
		expect = '''Program([VarDecl(Id(Zz5), ArrayType([59.36, 95.97, 1.32], StringType), None, None), FuncDecl(Id(LBZ), [VarDecl(Id(oll6), ArrayType([7.0], BoolType), None, None), VarDecl(Id(S9), ArrayType([51.84, 68.06], StringType), None, None)], Return()), FuncDecl(Id(vzq), [VarDecl(Id(CHRl), BoolType, None, None), VarDecl(Id(Rw), ArrayType([47.13, 52.54, 96.0], BoolType), None, None)], Return())])'''
		self.assertTrue(TestAST.test(input, expect, 399))

	def test_400(self):
		input = '''
func Bt (number en1[34.11,75.4], bool qvtm[65.24,56.79])	return false
func Tns (number zZr)	return

func r1V ()
	return

number y5a[2.37] <- 86.48
number Zb[73.78] <- "K"
'''
		expect = '''Program([FuncDecl(Id(Bt), [VarDecl(Id(en1), ArrayType([34.11, 75.4], NumberType), None, None), VarDecl(Id(qvtm), ArrayType([65.24, 56.79], BoolType), None, None)], Return(BooleanLit(False))), FuncDecl(Id(Tns), [VarDecl(Id(zZr), NumberType, None, None)], Return()), FuncDecl(Id(r1V), [], Return()), VarDecl(Id(y5a), ArrayType([2.37], NumberType), None, NumLit(86.48)), VarDecl(Id(Zb), ArrayType([73.78], NumberType), None, StringLit(K))])'''
		self.assertTrue(TestAST.test(input, expect, 400))
