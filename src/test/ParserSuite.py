import unittest
from TestUtils import TestParser

class ParserSuite(unittest.TestCase):
	def test_201(self):
		input = '''
func XyJ ()
	return 329
bool b1l2 <- u03T[NAKx, "'"'"iIK", "J'"'""]
bool cy[64.321E-00,43.993,2.336e+03]
## !/dh+cJ%nF,nY|G}L#-x
## )&@w"~`M1^(4,tD#
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 201))

	def test_202(self):
		input = '''
var qxf_[77e-73,24.800,479] <- 8e-89
string PD[592e95] <- a4
dynamic bUsH <- false
'''
		expect = '''Error on line 2 col 8: ['''
		self.assertTrue(TestParser.test(input, expect, 202))

	def test_203(self):
		input = '''
number id ## E[xt:J-[yhUb@s1#+
## u7se}`lhOp,
dynamic AyUw <- "t$MJ"
func BRS (string UFIn[2,788.355])	begin
		begin
			## gBh( 
			## yuO`
			## `b#}j7U
		end
	end
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 203))

	def test_204(self):
		input = '''
## ?rpc!
func zapW (string iE[6.742,68.931])
	begin
		break
	end

'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 204))

	def test_205(self):
		input = '''
## p(%s}_(AFvO]%TU
## wCfk/~V/86
'''
		expect = '''Error on line 4 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 205))

	def test_206(self):
		input = '''
dynamic JoqB[413]
'''
		expect = '''Error on line 2 col 12: ['''
		self.assertTrue(TestParser.test(input, expect, 206))

	def test_207(self):
		input = '''
dynamic vYQ[66.719,4e-20,85e01] <- 96.668
func XyKJ (dynamic Zr, dynamic Sy[4e-80,345.521])	return 449

## B5vpPnT^|q%<yjt(j.p
## ~xf6UA2ev:
func I5 (dynamic Ie[67e+13,764.044], string s8us)	begin
	end
'''
		expect = '''Error on line 2 col 11: ['''
		self.assertTrue(TestParser.test(input, expect, 207))

	def test_208(self):
		input = '''
func Y6yg (dynamic bAUv[73.839,26.900,1.598])	begin
		byLl()
	end

func KO ()	return

## *u?>3}+~!~#V&8hmiS
'''
		expect = '''Error on line 2 col 11: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 208))

	def test_209(self):
		input = '''
string MH2[73,1.653] ## d]fRo{,CNy1+
## U73L{koS$
func zW7Y (bool Il7, number Oj6B[9,9.072e87,5.847E-71])
	return

func FX3 (dynamic fpS[313,495,34.051e+22])
	return 408.192E54
'''
		expect = '''Error on line 7 col 10: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 209))

	def test_210(self):
		input = '''
number uW9
## ?;)Y<,OAq@:cyP%)S
## a+A
number ZncP <- 98.842E28
## ;
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 210))

	def test_211(self):
		input = '''
string oH ## %scj%[|V Z-PXpkL
func x6yo (bool Am, dynamic oyl, var cJ8R)
	return 62.048E+16
## E"
number CuC
'''
		expect = '''Error on line 3 col 20: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 211))

	def test_212(self):
		input = '''
string vC
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 212))

	def test_213(self):
		input = '''
## ~+@0uC?{]/eyq%8
## g  d]/.9=D"fGaC
dynamic PJ4[302.672,41.128] <- true
## ki4TyMO,x
## ,3.~KK>gyukSW0[
'''
		expect = '''Error on line 4 col 11: ['''
		self.assertTrue(TestParser.test(input, expect, 213))

	def test_214(self):
		input = '''
func X8DN ()	begin
	end
func fT (bool IV)
	return Qsqu
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 214))

	def test_215(self):
		input = '''
func e7 (dynamic g4[6e07], bool lnL)	return false

'''
		expect = '''Error on line 2 col 9: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 215))

	def test_216(self):
		input = '''
## )07*%:
'''
		expect = '''Error on line 3 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 216))

	def test_217(self):
		input = '''
## If%-x3
func Q_pP ()	return 52.354
string tLWw[28,3.878E-31] <- 0E+18
func Jg (bool SN[8.029e92])	return

'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 217))

	def test_218(self):
		input = '''
dynamic Qu ## [;-)@zo
var wkE ## DU7eY^v
'''
		expect = '''Error on line 3 col 19: 
'''
		self.assertTrue(TestParser.test(input, expect, 218))

	def test_219(self):
		input = '''
## cHzHsh^
'''
		expect = '''Error on line 3 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 219))

	def test_220(self):
		input = '''
func VXN (var UK1t[472.856E-94,361e-19])
	return true
## L>bFZ5UL!gkRcfklb#
func hF (bool Eiao[5.080E-07,56.798,46], bool KfmA[4E-81,336], bool rzE[941.276,115e71,29])
	return
'''
		expect = '''Error on line 2 col 10: var'''
		self.assertTrue(TestParser.test(input, expect, 220))

	def test_221(self):
		input = '''
## M2:$D^,s[FzZSXm[
## tX/3@c
'''
		expect = '''Error on line 4 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 221))

	def test_222(self):
		input = '''
func qvz (string c3h8, number LRH7)	return
## gozpi[c3e}
func rSx_ (bool WCG)
	begin
		begin
			## wPHhjGe<{~
			## GEr[~~+ZWQWpLEd~k
			for nhfs until "19'"^" by rx
				return
		end
	end

string cP[920.538E98,2.658e+38] <- "rF" ## V38t=p"
## fEG2hKs(9~Ph,
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 222))

	def test_223(self):
		input = '''
bool PPt[52.430e+85,8.975] ## ;T=cbLK^BMfU?Z7
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 223))

	def test_224(self):
		input = '''
func sqgX (var q_[7e89], bool tTuq, number KKvU)
	return "'""
func JeYp ()	return
func hP (var aJ[355.680,1.353,9E-59], bool bTV, number z5[99,6,784E+89])	begin
		## $].K/}~F<i-8
		## l
		## "s]~!PaxiVEW,J8
	end

'''
		expect = '''Error on line 2 col 11: var'''
		self.assertTrue(TestParser.test(input, expect, 224))

	def test_225(self):
		input = '''
## *-}uDKj
number UE <- YUT9 ## 85&)=1["=TV3T
func rS (number KOg)	return "'"'"65'""

number n0t
func SQf (string xTp[0,415], dynamic ii[1.232e+16,22e+01])	return

'''
		expect = '''Error on line 7 col 29: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 225))

	def test_226(self):
		input = '''
func ic (number bGI, string SE[192.125])
	return
func Q2 (var kTao, number BT)	begin
		## tgkj`
		H8g()
	end
## Q<dZgXQ
dynamic msA[82E-34,80.751,48.961] ## 1 |K;^7g
'''
		expect = '''Error on line 4 col 9: var'''
		self.assertTrue(TestParser.test(input, expect, 226))

	def test_227(self):
		input = '''
func AG (string Qt)	return NJL3

string e53k
func ZpC (number vqw[2E-77,20.172e-69,748.933], dynamic XudX[9.508e10,8.221E+97], var sNG)	return WWV

## P>ud$G
func W3v (number oa8)	return
'''
		expect = '''Error on line 5 col 48: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 227))

	def test_228(self):
		input = '''
string NIH <- false ## &[Opaeanotd^Slc@2x
## <."M2
## JsjU6
## L[Z8R
bool G17[598.392E-71,3e-96] ## yB.DDw X}E5XJd0[F
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 228))

	def test_229(self):
		input = '''
## x~Dp[EM3/j
## V4~OMa{Wh7F@"&.&0}
## J
string zlL ##  -T)uR
## uWJZB
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 229))

	def test_230(self):
		input = '''
func jGA (var yo[391.289E-25,4.765e40], dynamic uzT)
	return true
func WU (var QRYH[72], number IG2H[40.198E08], bool AFK)
	begin
		## 3U
		## d.3(
	end

func ZW (number v0pH[89e+63,2E+97], dynamic gvt)
	return
'''
		expect = '''Error on line 2 col 10: var'''
		self.assertTrue(TestParser.test(input, expect, 230))

	def test_231(self):
		input = '''
func uXKU ()
	begin
		## `3!,b
		return
		T8(true, CmP8, 94e-62)
	end
func Dnn ()	return

string Wr
func M_ (string Cy[1,78.116,4], number ZVTU, string aZWm)
	return
string D_ <- zd
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 231))

	def test_232(self):
		input = '''
## |BVw_|Rwe9o:e f{C_5z
dynamic XU3[98,2.944E-02] <- "'"'"h" ## 9<8$rKAZ^4f:5EC5)
## v*]`.drp&q9JLeNqJ
func mO ()
	return

func rniW (string AcsY, number Fqzj[35e+05,5.426e-09,8.793], bool Ha)
	return
'''
		expect = '''Error on line 3 col 11: ['''
		self.assertTrue(TestParser.test(input, expect, 232))

	def test_233(self):
		input = '''
var Sk[2e+47,41.188E+38] ## Dd
var FI_ <- "'"T'""
## >K%nD1@2$R*cGt{a"
## Ql#}g9fmqC
func auVv (dynamic ih7)
	begin
		return
		## hC~4Y67nUJM!UB|k
	end
'''
		expect = '''Error on line 2 col 6: ['''
		self.assertTrue(TestParser.test(input, expect, 233))

	def test_234(self):
		input = '''
func KyT (dynamic bF[569.560E+64,513.815e73], number rSc[2.925,6.793], dynamic FyZ[7.146E-49])	return "Y$;'""

## LU7G!)b,W(AP1^!F?L+*
'''
		expect = '''Error on line 2 col 10: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 234))

	def test_235(self):
		input = '''
func GO ()
	return
## _pc;XV-V= g;w*mnI
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 235))

	def test_236(self):
		input = '''
dynamic G3 <- false
var nq[83.972,2.412E+07]
func mX (dynamic AD76[307.145,384e+00,518])
	begin
		## [PP?iLaza|L%6
		begin
		end
		## *#c (5^V2~nzjXX*e
	end

'''
		expect = '''Error on line 3 col 6: ['''
		self.assertTrue(TestParser.test(input, expect, 236))

	def test_237(self):
		input = '''
func qn ()	return
dynamic pr[360.916,5e+63,60.594] <- cg
## VFM DVQiM=^OS5S70
'''
		expect = '''Error on line 3 col 10: ['''
		self.assertTrue(TestParser.test(input, expect, 237))

	def test_238(self):
		input = '''
var rHER[1E+20] ## *
## WFoU!NELY]n"L
number Kq[8.397,349.260E-41] ## ^:kl)+
bool H52s ## /o-Ud/
'''
		expect = '''Error on line 2 col 8: ['''
		self.assertTrue(TestParser.test(input, expect, 238))

	def test_239(self):
		input = '''
bool E2M2[1.279e86] ## iwb.=n;z2tZ(.
func rLGN (string Wdqt[2.895], string u0[71.598E+28,3])	return 156
func yY (string tI)	return

'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 239))

	def test_240(self):
		input = '''
## q^BH5!/^Yo.VZ?$>|
func WR3b ()	return
func dRdW (var GcHQ[155.997E-44,141.037,33], dynamic t5E[559E+89], var Q4MI)
	return
## <2h-Of
'''
		expect = '''Error on line 4 col 11: var'''
		self.assertTrue(TestParser.test(input, expect, 240))

	def test_241(self):
		input = '''
## W<~J]`AN_P;,4gg6EK/f
## 5h_4k]Qv7PTS{h
## xWX8P8y{s,?eBPe]F#s
'''
		expect = '''Error on line 5 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 241))

	def test_242(self):
		input = '''
func Gvt (dynamic CYE, var mKq[379.329,19.776e-81])
	begin
		## -@&2q:# Ra4
		fmP()
	end

number EDOg <- Sjm ## H+S<3,?NqTM
## k1.e
## fVaG?cO|
## B&4M_!S
'''
		expect = '''Error on line 2 col 10: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 242))

	def test_243(self):
		input = '''
## ,3k|R
number TKFx[3,484e14] <- "'"N"
dynamic SXe5
func rh (dynamic VJSU[975])
	begin
		Zo("'"'"", false)[false, synv, bmB] <- 6.682e18
	end

'''
		expect = '''Error on line 5 col 9: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 243))

	def test_244(self):
		input = '''
number nn <- false ## |y_w_ 
func j4Z (bool YdG9)	return
func irBa (var FO[2])	return true
## ZBIxL3HE0D
'''
		expect = '''Error on line 4 col 11: var'''
		self.assertTrue(TestParser.test(input, expect, 244))

	def test_245(self):
		input = '''
func TG ()
	return
func zjLN (bool hb8[527.459E-92,79E+46], bool GRcj)	begin
	end

'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 245))

	def test_246(self):
		input = '''
## b
var vExY[4.377,799E+10,40e-87] <- true ## im[VJE#y>
'''
		expect = '''Error on line 3 col 8: ['''
		self.assertTrue(TestParser.test(input, expect, 246))

	def test_247(self):
		input = '''
func ih (bool dv9f[9,48e-47,8.524e+65], string VFX[613], number F2E[180E-26,7.090])
	return nAe

string EA1E <- false ## 1 GN5J6Mhu^,T il9c0<
func DO (bool AVQ)	return "?3'"'""

'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 247))

	def test_248(self):
		input = '''
func jS7 (dynamic FLR[0E+98,2], bool xj, dynamic Nr9)	return a5uL
'''
		expect = '''Error on line 2 col 10: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 248))

	def test_249(self):
		input = '''
number PqhV <- Z6L ## ;<rbg!g
bool zk[52,77E+06] ## zIJTT<X{Z+@
var JKgF[2.940E+01]
dynamic Pr[8.513,9.677] <- false ## y%F&##=NX9Pcy*4ZW"
'''
		expect = '''Error on line 4 col 8: ['''
		self.assertTrue(TestParser.test(input, expect, 249))

	def test_250(self):
		input = '''
string LOh <- Yce
func Y0 (bool d0, var bk3[9,41.093E-88,6.870])	begin
	end
dynamic aE[83.319e26,300.866] <- false
## <<B_T
'''
		expect = '''Error on line 3 col 18: var'''
		self.assertTrue(TestParser.test(input, expect, 250))

	def test_251(self):
		input = '''
func O8X ()
	return

## +0
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 251))

	def test_252(self):
		input = '''
var L6 ## ~d=^H!FcuA~:
func EU ()	begin
		break
		return
	end
func sH (number oj, var SK4, bool FuIN)
	begin
	end
func isdb ()
	return

## 6;}~i
'''
		expect = '''Error on line 2 col 23: 
'''
		self.assertTrue(TestParser.test(input, expect, 252))

	def test_253(self):
		input = '''
func JN4 (string EII)	return
func Vv (number nLoC[60.232], string GS)	return true
func O4A (number Lz, number JG[159.048E83,8.855e+94,6e-66], string fv[2e35,72e+24,3.411e-15])	return
var JPIq[546.841E+49,170] ## ?-
'''
		expect = '''Error on line 5 col 8: ['''
		self.assertTrue(TestParser.test(input, expect, 253))

	def test_254(self):
		input = '''
## ~&1@pwC@B2fRXg_
## Kam4a?dtP)}cb<6sX"M
var RoGF[546.781,316e-52] <- "8dOj"
bool aCh[7.779E-59,919.842E+96] <- tMub ## O#nRS$
'''
		expect = '''Error on line 4 col 8: ['''
		self.assertTrue(TestParser.test(input, expect, 254))

	def test_255(self):
		input = '''
func Yey ()	return
func bW_1 (string rywJ[77e+92])
	return xizc
## q:]M`F,i,,$:m"$+
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 255))

	def test_256(self):
		input = '''
## [hcGh]W7X:gOU^3O
dynamic pD3p <- 899.685
func X0 (bool AB[92,791.374e+30])
	begin
		## H/
		for Ag_u until 475.180 by qVa
			number rT3[49.474e+87] ## f?#>=3
		## }G*d8MN=GlS#JmVl#b5N
	end

'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 256))

	def test_257(self):
		input = '''
## D&{HO.vomZ82
## :}<L}kq1
bool MD6[94e-69,224.096e72] <- 2
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 257))

	def test_258(self):
		input = '''
string OS ## UmhR#jtAAR$+
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 258))

	def test_259(self):
		input = '''
dynamic ZW8f[1E-68] ## s6YuiF2mGUB=
func Tw (string G9[52,7E28,69], var LA[823.328e31])
	begin
		## (bAvd^7u_u?>%nT8
		## y iQP//O.AZ%v
	end
## yf)Z3!
bool yWLy <- z7 ## y26Y}l,$Mh(aV
'''
		expect = '''Error on line 2 col 12: ['''
		self.assertTrue(TestParser.test(input, expect, 259))

	def test_260(self):
		input = '''
func EMQ9 (var jmw[43.585], bool wZn)
	return
'''
		expect = '''Error on line 2 col 11: var'''
		self.assertTrue(TestParser.test(input, expect, 260))

	def test_261(self):
		input = '''
func Kf_3 (bool Qd5[399.426])
	begin
		## vl~$b?%tKoVZMIDp
	end
var jtRV
func VzK (var dwMq[1e+15])
	begin
		## {L*{V:0k/P0%>B4
		## rWDU1mOAp]7F~77
	end

'''
		expect = '''Error on line 6 col 9: 
'''
		self.assertTrue(TestParser.test(input, expect, 261))

	def test_262(self):
		input = '''
## V"
'''
		expect = '''Error on line 3 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 262))

	def test_263(self):
		input = '''
func TOA2 ()	return

## puNg`d-
number zz[22.345e+02] <- 68.070E+76
var X0Km <- "'"'","
func GQL (number zT[6.071E+29,20e+24,2], var tZ[4.606e-07,787.044,8.571], string SIwm[9.117E+35,17.831e+81,823e81])
	return

'''
		expect = '''Error on line 7 col 41: var'''
		self.assertTrue(TestParser.test(input, expect, 263))

	def test_264(self):
		input = '''
string BCtQ[8.135E+26,5.451E79,45e01]
## f-p V^aG7
func fup ()
	return
bool oLf[27E-92,656.633,4.052E75] ## %,c25rZ?C>^eIz.u5-
func pEh_ (string Q4oJ, dynamic BwB[405.068])	begin
		## 7!]<~}zv<?dN#C{b
		string l0n[14,0.463e-02,78.841] <- "'"'"'"wP" ## S/Q$j%ji~eC1D#]</J
	end

'''
		expect = '''Error on line 7 col 24: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 264))

	def test_265(self):
		input = '''
bool ob[2E+36,5] ## D7&?yRh)T_?##s>
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 265))

	def test_266(self):
		input = '''
## SPUr
func I0 ()
	begin
		begin
			## #$Hw
			## S!aoOjj
			## y#~W([_Wzd
		end
	end
var un ## cB;-=.78#h+>SRH.
'''
		expect = '''Error on line 11 col 27: 
'''
		self.assertTrue(TestParser.test(input, expect, 266))

	def test_267(self):
		input = '''
dynamic Ge[9e12,58.095E-93,5E-31] <- 809 ## ]q!&H%_
func czu (dynamic A6Q, string JM, bool D3dE)
	return BBR_
func Cq (number cml)
	return

## oPb@6<.|sF?I
'''
		expect = '''Error on line 2 col 10: ['''
		self.assertTrue(TestParser.test(input, expect, 267))

	def test_268(self):
		input = '''
bool Q0JS[1E-10,63e-66] <- EOL
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 268))

	def test_269(self):
		input = '''
func ASKx (var R6F[53.211], string tTF)
	return D3y
number eErc <- "#4" ## J*#4~K@Y-iL$<K
var lzT1[207,375.647,431.354] ## mKcqesp
dynamic QQ[10.993,6E+01]
'''
		expect = '''Error on line 2 col 11: var'''
		self.assertTrue(TestParser.test(input, expect, 269))

	def test_270(self):
		input = '''
## wpjdD#/Fwv%`v
number Iz[4E-09]
## xNgOnp=Bi&
func aS_ ()	begin
	end

'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 270))

	def test_271(self):
		input = '''
## g5X$0[?hZZG+
## )KzCx.:{6#KcsQ9[
'''
		expect = '''Error on line 4 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 271))

	def test_272(self):
		input = '''
dynamic EPwK <- true ## JAB qTk
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 272))

	def test_273(self):
		input = '''
var zs <- "xM'"Rl"
number aZFy[14.017E+73,5.290] <- "'"'"'"'"" ## #Q
## ]b
string Bq1[22,9.118,73.959e+29] <- 7
func xFPC ()	return "'""

'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 273))

	def test_274(self):
		input = '''
func Z4K (string yaTl, bool yTp[35,87.365])	return

## c~5SV0bubg
func DYQ (var se[19E+10], var m2U[30,85.971e79], dynamic O7k)	return

func BzV ()	begin
	end

bool hG
'''
		expect = '''Error on line 5 col 10: var'''
		self.assertTrue(TestParser.test(input, expect, 274))

	def test_275(self):
		input = '''
number eHsZ <- ukcV ## hk>HI
number nbcs
func jPDc ()
	begin
		var TC[24E11] ## -D%i2ud6.o_3]n"
		## 7/o^%z&XWed21>P&
	end
'''
		expect = '''Error on line 6 col 8: ['''
		self.assertTrue(TestParser.test(input, expect, 275))

	def test_276(self):
		input = '''
func nQL (string qIUI[65], dynamic ai, string npyM)	begin
		Zut(191, obta)["'"N", KA6V] <- "'"2'""
		## 2{U@A3LGH.hQ7
		TX5()
	end

## @m^{/-f8;ZoUn
var xLa
'''
		expect = '''Error on line 2 col 27: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 276))

	def test_277(self):
		input = '''
var Mr[285,8.641] <- 345E+51 ## y Z9k 
bool bNmH[1.337E10,8E67] <- "'"6'"'""
'''
		expect = '''Error on line 2 col 6: ['''
		self.assertTrue(TestParser.test(input, expect, 277))

	def test_278(self):
		input = '''
dynamic b2[95.310,401e+62] <- rW2Y
'''
		expect = '''Error on line 2 col 10: ['''
		self.assertTrue(TestParser.test(input, expect, 278))

	def test_279(self):
		input = '''
func BOXV (string EBa)	begin
	end

func nWn (var PV[830,8.789E-64], dynamic Ts, var WISP[6.950])
	begin
		break
		return xGM2
	end

## vo?L4g?[U
'''
		expect = '''Error on line 5 col 10: var'''
		self.assertTrue(TestParser.test(input, expect, 279))

	def test_280(self):
		input = '''
## %#/t-hb=^]+pq?[%NT
## Q*-|^V^=o~C^
## jEPH)l*;4wE
func GLsi ()
	begin
		## OBr TJiRDn==Y-0QV
		XU[BYfz] <- eQ
		## #Zcwz$qu?Y<|R3qD6z
	end

'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 280))

	def test_281(self):
		input = '''
## ;O6
dynamic kG[8E+07,54.734e01,65.230] ## I~`ST!q]7_ZvLE}U.5
## hr`-&&)K?aS
number e9CS <- 3.954E-87 ## VpI83""/[~zK
'''
		expect = '''Error on line 3 col 10: ['''
		self.assertTrue(TestParser.test(input, expect, 281))

	def test_282(self):
		input = '''
func qQp0 (string Ap9[318,6.070E-12,401E+41], var GY[28e34,99e77,33.095])
	return

## N|gbL&(2v}5Z&Y yx7P%
number FsNk[9.497E+54] <- "'"h'"'"V" ## KrX+5zjphH^b`>
'''
		expect = '''Error on line 2 col 46: var'''
		self.assertTrue(TestParser.test(input, expect, 282))

	def test_283(self):
		input = '''
func Ow1 (number QMB3)
	begin
	end

'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 283))

	def test_284(self):
		input = '''
bool w9yi <- 89E43
## QfgqYn9
bool ttaW ## U/BBFh
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 284))

	def test_285(self):
		input = '''
func U1X9 (number dP[7,645e+55], number wFL)
	return
string lG
## )(p>B"
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 285))

	def test_286(self):
		input = '''
func Nzpu ()	begin
		## 1Asp7G2~Yj_+Uts=9?ul
		## &*[]pR)R
	end
func a3 ()	return

string cM[67.393E+61]
number s0hv <- 990.900e76 ## j
func rX (dynamic QEPx, number JXn, bool JEG)
	begin
		## yil$
	end
'''
		expect = '''Error on line 10 col 9: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 286))

	def test_287(self):
		input = '''
string ywj6 <- 0.255
func KF7 (dynamic IcKs)
	begin
		## Wxx>Jg{~Jtk04
	end

## &vx,|4)ABhqYRq
'''
		expect = '''Error on line 3 col 10: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 287))

	def test_288(self):
		input = '''
## =M[0IbI)e.acV-
'''
		expect = '''Error on line 3 col 0: <EOF>'''
		self.assertTrue(TestParser.test(input, expect, 288))

	def test_289(self):
		input = '''
func YrsU ()
	begin
		## C,OvHu`)a6yDT77+uGhE
		return
		break
	end
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 289))

	def test_290(self):
		input = '''
func PE (bool Uoa[4E58,20.125E-15], number Iw)
	begin
		break
		YAre[false, true, 19.436] <- false
	end
func mM (bool eA[8E+70,9E-14,5e95])
	begin
	end
func IL (dynamic nW, bool xX[3.175e+25,6,228e30])
	return

func Rr (var ypb[20E+10,72.179])	return

func zSI (string UmT, string cew[735E-12])	begin
	end

'''
		expect = '''Error on line 10 col 9: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 290))

	def test_291(self):
		input = '''
number Gyb <- "'"'""
## qdpQ]
string mJk[59,127.534] <- 98.488E-66 ## l=>Q
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 291))

	def test_292(self):
		input = '''
func eZ ()
	return 9e-58
dynamic Se ## g=wt6-_-RcC;"~BZK6)
func i4 (dynamic ZXK)
	return

'''
		expect = '''Error on line 5 col 9: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 292))

	def test_293(self):
		input = '''
func OxZ ()
	return f1

## Wjq+}>(=nsAOBT4
## Rc
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 293))

	def test_294(self):
		input = '''
## H
func Hvi (string MYb, number zgyR, dynamic dn8[3.810])
	return

bool EUEY
'''
		expect = '''Error on line 3 col 35: dynamic'''
		self.assertTrue(TestParser.test(input, expect, 294))

	def test_295(self):
		input = '''
func ulZM (var sZ_Q)
	begin
		## D:]q
		## J
	end
'''
		expect = '''Error on line 2 col 11: var'''
		self.assertTrue(TestParser.test(input, expect, 295))

	def test_296(self):
		input = '''
## dw>V@`
var orE2 <- HfCf ## HG]Qgc2mU(.#e81B|:b
## `n3H
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 296))

	def test_297(self):
		input = '''
func Opa ()
	begin
		if ("<'"'"V") if (55E13)
		GV8 <- 985e-06
		else break
		elif (43.791e71)
		v1Di()["'"CbS'""] <- 45.906e+55
		elif (5) xRQ("'"n")["'"Z'"'"v"] <- MY
		elif (9)
		dynamic rG <- "'"'""
		elif (A5)
		H6m()
		else begin
			bnDc(false, false, 1)["N>"] <- 29E+72
		end
		continue
	end
'''
		expect = '''Error on line 8 col 8: ['''
		self.assertTrue(TestParser.test(input, expect, 297))

	def test_298(self):
		input = '''
bool eM
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 298))

	def test_299(self):
		input = '''
## -Vw_,(x
var SwO
## QY
## q1mu"tpMib-:|Qw*,MJ
func W2SJ (dynamic D9)	return "-'"'""

'''
		expect = '''Error on line 3 col 8: 
'''
		self.assertTrue(TestParser.test(input, expect, 299))

	def test_300(self):
		input = '''
## lpWw|=gusCZq[uq6w`=m
func LYcX ()
	begin
	end
number hro ## iZxrv[J75rAd$j
'''
		expect = '''successful'''
		self.assertTrue(TestParser.test(input, expect, 300))
