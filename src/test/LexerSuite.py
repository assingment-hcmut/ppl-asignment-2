import unittest
from TestUtils import TestLexer

class LexerSuite(unittest.TestCase):
	def test_101(self):
		self.assertTrue(TestLexer.test('''## gh>%)0a@kUZglB-M|s~''', '''<EOF>''', 101))

	def test_102(self):
		self.assertTrue(TestLexer.test('''## f9G>a''', '''<EOF>''', 102))

	def test_103(self):
		self.assertTrue(TestLexer.test('''"'"d"''', '''\'"d,<EOF>''', 103))

	def test_104(self):
		self.assertTrue(TestLexer.test("Irpl", "Irpl,<EOF>", 104))

	def test_105(self):
		self.assertTrue(TestLexer.test("&F", "Error Token &", 105))

	def test_106(self):
		self.assertTrue(TestLexer.test('''"t'"'"'" ''', '''Unclosed String: t'"'"'" ''', 106))

	def test_107(self):
		self.assertTrue(TestLexer.test("mLJq", "mLJq,<EOF>", 107))

	def test_108(self):
		self.assertTrue(TestLexer.test('''" \\jA'""''', '''Illegal Escape In String:  \\j''', 108))

	def test_109(self):
		self.assertTrue(TestLexer.test("uPYZBsVW4", "uPYZBsVW4,<EOF>", 109))

	def test_110(self):
		self.assertTrue(TestLexer.test('''"5'"'"'"\\k'""''', '''Illegal Escape In String: 5'"'"'"\\k''', 110))

	def test_111(self):
		self.assertTrue(TestLexer.test("75E+03", "75E+03,<EOF>", 111))

	def test_112(self):
		self.assertTrue(TestLexer.test("2E-14", "2E-14,<EOF>", 112))

	def test_113(self):
		self.assertTrue(TestLexer.test("JS$1z54zVH", "JS,Error Token $", 113))

	def test_114(self):
		self.assertTrue(TestLexer.test("749.968E-51", "749.968E-51,<EOF>", 114))

	def test_115(self):
		self.assertTrue(TestLexer.test('''## 2zLcoB''', '''<EOF>''', 115))

	def test_116(self):
		self.assertTrue(TestLexer.test("25.524", "25.524,<EOF>", 116))

	def test_117(self):
		self.assertTrue(TestLexer.test("YnSB", "YnSB,<EOF>", 117))

	def test_118(self):
		self.assertTrue(TestLexer.test('''"
'""''', '''Unclosed String: ''', 118))

	def test_119(self):
		self.assertTrue(TestLexer.test("Y", "Y,<EOF>", 119))

	def test_120(self):
		self.assertTrue(TestLexer.test("17", "17,<EOF>", 120))

	def test_121(self):
		self.assertTrue(TestLexer.test('''"Fbd'"Y"''', '''Fbd'"Y,<EOF>''', 121))

	def test_122(self):
		self.assertTrue(TestLexer.test('''"v'"E'""''', '''v'"E'",<EOF>''', 122))

	def test_123(self):
		self.assertTrue(TestLexer.test('''"0 ''', '''Unclosed String: 0 ''', 123))

	def test_124(self):
		self.assertTrue(TestLexer.test('''## R*FTb08&rq,K+,WE1''', '''<EOF>''', 124))

	def test_125(self):
		self.assertTrue(TestLexer.test('''## `<h--"e7KY''', '''<EOF>''', 125))

	def test_126(self):
		self.assertTrue(TestLexer.test('''## TP4NOK"x"Ti9S3''', '''<EOF>''', 126))

	def test_127(self):
		self.assertTrue(TestLexer.test("^3LQ9_FbE", "Error Token ^", 127))

	def test_128(self):
		self.assertTrue(TestLexer.test('''"'"k"''', '''\'"k,<EOF>''', 128))

	def test_129(self):
		self.assertTrue(TestLexer.test("WrA&3Q", "WrA,Error Token &", 129))

	def test_130(self):
		self.assertTrue(TestLexer.test("cWSJCJ", "cWSJCJ,<EOF>", 130))

	def test_131(self):
		self.assertTrue(TestLexer.test("61e87", "61e87,<EOF>", 131))

	def test_132(self):
		self.assertTrue(TestLexer.test("6.237e-99", "6.237e-99,<EOF>", 132))

	def test_133(self):
		self.assertTrue(TestLexer.test("rJAPV5X0M", "rJAPV5X0M,<EOF>", 133))

	def test_134(self):
		self.assertTrue(TestLexer.test('''"\\g7G! '""''', '''Illegal Escape In String: \\g''', 134))

	def test_135(self):
		self.assertTrue(TestLexer.test("777", "777,<EOF>", 135))

	def test_136(self):
		self.assertTrue(TestLexer.test('''"M'"'""''', '''M'"'",<EOF>''', 136))

	def test_137(self):
		self.assertTrue(TestLexer.test('''## x0RnjVlVbA. 6''', '''<EOF>''', 137))

	def test_138(self):
		self.assertTrue(TestLexer.test('''""''', ''',<EOF>''', 138))

	def test_139(self):
		self.assertTrue(TestLexer.test("1M8j6PTo^@", "1,M8j6PTo,Error Token ^", 139))

	def test_140(self):
		self.assertTrue(TestLexer.test('''""''', ''',<EOF>''', 140))

	def test_141(self):
		self.assertTrue(TestLexer.test('''"
H'"'"@'""''', '''Unclosed String: ''', 141))

	def test_142(self):
		self.assertTrue(TestLexer.test('''"JP?'" ''', '''Unclosed String: JP?'" ''', 142))

	def test_143(self):
		self.assertTrue(TestLexer.test("26e-56", "26e-56,<EOF>", 143))

	def test_144(self):
		self.assertTrue(TestLexer.test("OND", "OND,<EOF>", 144))

	def test_145(self):
		self.assertTrue(TestLexer.test("6eHcs&s", "6,eHcs,Error Token &", 145))

	def test_146(self):
		self.assertTrue(TestLexer.test('''"'"'"m"''', '''\'"'"m,<EOF>''', 146))

	def test_147(self):
		self.assertTrue(TestLexer.test("5.738e+48", "5.738e+48,<EOF>", 147))

	def test_148(self):
		self.assertTrue(TestLexer.test("413.952E54", "413.952E54,<EOF>", 148))

	def test_149(self):
		self.assertTrue(TestLexer.test('''## c=5tDH%OmQgms/IDfk5''', '''<EOF>''', 149))

	def test_150(self):
		self.assertTrue(TestLexer.test("JADYVhKRw2", "JADYVhKRw2,<EOF>", 150))

	def test_151(self):
		self.assertTrue(TestLexer.test("4", "4,<EOF>", 151))

	def test_152(self):
		self.assertTrue(TestLexer.test('''## ou^A-1o''', '''<EOF>''', 152))

	def test_153(self):
		self.assertTrue(TestLexer.test("ONcSEcGU", "ONcSEcGU,<EOF>", 153))

	def test_154(self):
		self.assertTrue(TestLexer.test("65.132E13", "65.132E13,<EOF>", 154))

	def test_155(self):
		self.assertTrue(TestLexer.test("@pgeYkU", "Error Token @", 155))

	def test_156(self):
		self.assertTrue(TestLexer.test("qh", "qh,<EOF>", 156))

	def test_157(self):
		self.assertTrue(TestLexer.test("2.585e75", "2.585e75,<EOF>", 157))

	def test_158(self):
		self.assertTrue(TestLexer.test("R01WeFs", "R01WeFs,<EOF>", 158))

	def test_159(self):
		self.assertTrue(TestLexer.test("WoNm", "WoNm,<EOF>", 159))

	def test_160(self):
		self.assertTrue(TestLexer.test("28.627E-51", "28.627E-51,<EOF>", 160))

	def test_161(self):
		self.assertTrue(TestLexer.test('''"'"Z\\j1b'""''', '''Illegal Escape In String: '"Z\\j''', 161))

	def test_162(self):
		self.assertTrue(TestLexer.test('''## ^j,$.^=ldZ0''', '''<EOF>''', 162))

	def test_163(self):
		self.assertTrue(TestLexer.test("_oHUu3$", "_oHUu3,Error Token $", 163))

	def test_164(self):
		self.assertTrue(TestLexer.test('''"'"'"4
'""''', '''Unclosed String: '"'"4''', 164))

	def test_165(self):
		self.assertTrue(TestLexer.test('''"'"'"\\p'""''', '''Illegal Escape In String: '"'"\\p''', 165))

	def test_166(self):
		self.assertTrue(TestLexer.test("978", "978,<EOF>", 166))

	def test_167(self):
		self.assertTrue(TestLexer.test('''"
/'"'"z"''', '''Unclosed String: ''', 167))

	def test_168(self):
		self.assertTrue(TestLexer.test("667.597", "667.597,<EOF>", 168))

	def test_169(self):
		self.assertTrue(TestLexer.test('''## KL,mWV8LH/t*''', '''<EOF>''', 169))

	def test_170(self):
		self.assertTrue(TestLexer.test("T5s", "T5s,<EOF>", 170))

	def test_171(self):
		self.assertTrue(TestLexer.test("Ks", "Ks,<EOF>", 171))

	def test_172(self):
		self.assertTrue(TestLexer.test("26", "26,<EOF>", 172))

	def test_173(self):
		self.assertTrue(TestLexer.test("9y@", "9,y,Error Token @", 173))

	def test_174(self):
		self.assertTrue(TestLexer.test("1.771E32", "1.771E32,<EOF>", 174))

	def test_175(self):
		self.assertTrue(TestLexer.test('''"O'"
K'""''', '''Unclosed String: O'"''', 175))

	def test_176(self):
		self.assertTrue(TestLexer.test("84E-30", "84E-30,<EOF>", 176))

	def test_177(self):
		self.assertTrue(TestLexer.test("V#$E", "V,Error Token #", 177))

	def test_178(self):
		self.assertTrue(TestLexer.test('''## z<!N:afwGz{p#:HK''', '''<EOF>''', 178))

	def test_179(self):
		self.assertTrue(TestLexer.test('''## jOlvI!9''', '''<EOF>''', 179))

	def test_180(self):
		self.assertTrue(TestLexer.test("Hat958RH7x", "Hat958RH7x,<EOF>", 180))

	def test_181(self):
		self.assertTrue(TestLexer.test("s&sJqf", "s,Error Token &", 181))

	def test_182(self):
		self.assertTrue(TestLexer.test('''"
'""''', '''Unclosed String: ''', 182))

	def test_183(self):
		self.assertTrue(TestLexer.test('''""''', ''',<EOF>''', 183))

	def test_184(self):
		self.assertTrue(TestLexer.test('''## 8+c~(Pw=__pncSMK&''', '''<EOF>''', 184))

	def test_185(self):
		self.assertTrue(TestLexer.test('''"'""''', '''\'",<EOF>''', 185))

	def test_186(self):
		self.assertTrue(TestLexer.test("Bi&aVbDrqa", "Bi,Error Token &", 186))

	def test_187(self):
		self.assertTrue(TestLexer.test('''##  <ITitQ.|)''', '''<EOF>''', 187))

	def test_188(self):
		self.assertTrue(TestLexer.test("sI", "sI,<EOF>", 188))

	def test_189(self):
		self.assertTrue(TestLexer.test("148.908e+84", "148.908e+84,<EOF>", 189))

	def test_190(self):
		self.assertTrue(TestLexer.test('''""''', ''',<EOF>''', 190))

	def test_191(self):
		self.assertTrue(TestLexer.test("573.881", "573.881,<EOF>", 191))

	def test_192(self):
		self.assertTrue(TestLexer.test("392", "392,<EOF>", 192))

	def test_193(self):
		self.assertTrue(TestLexer.test("B1u", "B1u,<EOF>", 193))

	def test_194(self):
		self.assertTrue(TestLexer.test('''## 3izzW&''', '''<EOF>''', 194))

	def test_195(self):
		self.assertTrue(TestLexer.test('''""''', ''',<EOF>''', 195))

	def test_196(self):
		self.assertTrue(TestLexer.test('''"$
lI'""''', '''Unclosed String: $''', 196))

	def test_197(self):
		self.assertTrue(TestLexer.test('''"'"'"~"''', '''\'"'"~,<EOF>''', 197))

	def test_198(self):
		self.assertTrue(TestLexer.test('''"R\\uS'""''', '''Illegal Escape In String: R\\u''', 198))

	def test_199(self):
		self.assertTrue(TestLexer.test('''## YM6?BU}?Au@!ZjGvdd,''', '''<EOF>''', 199))

	def test_200(self):
		self.assertTrue(TestLexer.test('''"'"\\jVM:'""''', '''Illegal Escape In String: '"\\j''', 200))
